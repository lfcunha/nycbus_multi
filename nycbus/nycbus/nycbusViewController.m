//
//  nycbusViewController.m
//  nycbus
//
//  Created by Luis Cunha on 2/7/14.
//  Copyright (c) 2014 Luis Cunha. All rights reserved.
//

#import "nycbusViewController.h"

@interface nycbusViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *userLocationLabel;
@property (nonatomic, retain) CoreLocationController *locationController;

@end

@implementation nycbusViewController
- (IBAction)grammy {
    self.mapView.delegate = self;
    CLLocationCoordinate2D denverLocation = CLLocationCoordinate2DMake(42.0223667, -071.4927833);
    self.mapView.region =
    MKCoordinateRegionMakeWithDistance(denverLocation, 1000,1000);
}
- (IBAction)centerButton {
  //  CLLocationCoordinate2D currentLocation = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
  //  self.mapView.region =
  //  MKCoordinateRegionMakeWithDistance(currentLocation, 10000, 10000);
    if ([CLLocationManager locationServicesEnabled])
    {
        self.mapView.showsUserLocation = YES;
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    }
}



-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    self.userLocationLabel.text =
    [NSString stringWithFormat:@" Location: %.5f°, %.5f°",
     userLocation.coordinate.latitude, userLocation.coordinate.longitude];
}


//(void)update:(CLLocation *)location {
    //self.lblLatitude.text= [NSString stringWithFormat:@"Latitude: %f", [location coordinate].latitude];
    //self.lblLongitude.text = [NSString stringWithFormat:@"Longitude: %f", [location coordinate].longitude];
//}

- (void)locationError:(NSError *)error {
    //self.lblLatitude.text = [error description];
    //self.lblLongitude.text = nil;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.mapView.delegate = self;
    CLLocationCoordinate2D denverLocation = CLLocationCoordinate2DMake(0, 0);
    self.mapView.region =
    MKCoordinateRegionMakeWithDistance(denverLocation, 10000,10000);
    //39.739, -104.984
    
    
    
    
    //self.mapView.zoomEnabled = NO;
    //   self.mapView.scrollEnabled = NO;
    
    //Control User Location on Map
    if ([CLLocationManager locationServicesEnabled])
    {
        self.mapView.showsUserLocation = YES;
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    

   //     self.locationController = [[CoreLocationController alloc] init];
   //     self.locationController.delegate = self;
   //     [self.locationController.locationManager startUpdatingLocation];
        
        
    
    }
    

    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
