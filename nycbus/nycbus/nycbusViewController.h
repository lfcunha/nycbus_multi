//
//  nycbusViewController.h
//  nycbus
//
//  Created by Luis Cunha on 2/7/14.
//  Copyright (c) 2014 Luis Cunha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CoreLocationController.h"

@interface nycbusViewController : UIViewController <MKMapViewDelegate>



@end
