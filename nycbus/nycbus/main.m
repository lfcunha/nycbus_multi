//
//  main.m
//  nycbus
//
//  Created by Luis Cunha on 2/7/14.
//  Copyright (c) 2014 Luis Cunha. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "nycbusAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([nycbusAppDelegate class]));
    }
}
