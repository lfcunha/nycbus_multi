//
//  nycbusAppDelegate.h
//  nycbus
//
//  Created by Luis Cunha on 2/7/14.
//  Copyright (c) 2014 Luis Cunha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface nycbusAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
