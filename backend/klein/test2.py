import treq
from klein import Klein
from twisted.internet.defer import inlineCallbacks, returnValue
 
import time
app = Klein()
 
 
@app.route('/', methods = ['GET'])
@inlineCallbacks
def square_submit(request):
   x = int(request.args.get('x', [0])[0])
 
   ## simulate calling out to some Web service:
   ##
   r = yield treq.get('http://www.tavendo.com/')
   content = yield r.content()
   y = len(content)

   time.sleep(5)
   t = time.time() 
   res = x * x + y
   returnValue("{} squared plus {} is {} and time now is {}".format(x, y, res, t))
 
 
if __name__ == "__main__":
   app.run("localhost", 8081)
