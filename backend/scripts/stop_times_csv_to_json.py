import json

"""
data source:
http://transitfeeds.com/link?u=http://web.mta.info/developers/data/nyct/bus/google_transit_queens.zip
http://transitfeeds.com/link?u=http://web.mta.info/developers/data/nyct/bus/google_transit_brooklyn.zip
http://transitfeeds.com/link?u=http://web.mta.info/developers/data/nyct/bus/google_transit_staten_island.zip
http://transitfeeds.com/link?u=http://web.mta.info/developers/data/nyct/bus/google_transit_manhattan.zip
http://transitfeeds.com/link?u=http://web.mta.info/developers/data/nyct/bus/google_transit_manhattan.zip
"""



#convert stop_times csv to json
f = open("stop_times.csv", "r")

trip = {}
lines = f.readlines()

for line in lines:
	l = line.strip().split(",")
	if l[0] not in trip:
		trip[l[0]] = []
	trip[l[0]].append({"stop_sequence": l[4], arrival_time":l[1], "departure_time":l[2], "stop_id":l[3]})


#write to file
json.dump(trip, open("stop_times.json", "w"))

#check no duplicates
trip1 = {}
for line in lines:
	l = line.strip().split(",")
	if l[0] not in trip:
		trip[l[0]] = {}
	if l[4] in trip[l[0]]:
		print l
	else:
		trip[l[0]][l[4]] = 1
