#!/usr/bin/env bash

#run from database server
#make sure to have mysql config file ~/.my.cnf
#[client]
## for local server
#host=localhost
#user=lfcunha
#password=<passw>
                    

wget http://web.mta.info/developers/data/nyct/bus/google_transit_queens.zip
wget http://web.mta.info/developers/data/nyct/bus/google_transit_brooklyn.zip
wget http://web.mta.info/developers/data/nyct/bus/google_transit_staten_island.zip
wget http://web.mta.info/developers/data/nyct/bus/google_transit_bronx.zip 
wget http://web.mta.info/developers/data/nyct/bus/google_transit_manhattan.zip

#... + remove header line
mkdir google_transit_queens; mv google_transit_queens.zip google_transit_queens; cd google_transit_queens; unzip google_transit_queens.zip; sed '1d' stop_times.txt > tempfile; mv tempfile stop_times.txt; cd ../
mkdir google_transit_staten_island; mv google_transit_staten_island.zip google_transit_staten_island; cd google_transit_staten_island; unzip google_transit_staten_island.zip; sed '1d' stop_times.txt > tempfile; mv tempfile stop_times.txt; cd ../
mkdir google_transit_brooklyn; mv google_transit_brooklyn.zip google_transit_brooklyn; cd google_transit_brooklyn; unzip google_transit_brooklyn.zip; sed '1d' stop_times.txt > tempfile; mv tempfile stop_times.txt; cd ../
mkdir google_transit_bronx; mv google_transit_bronx.zip google_transit_bronx; cd google_transit_bronx; unzip google_transit_bronx.zip; sed '1d' stop_times.txt > tempfile; mv tempfile stop_times.txt; cd ../
mkdir google_transit_manhattan; mv google_transit_manhattan.zip google_transit_manhattan; cd google_transit_manhattan; unzip google_transit_manhattan.zip; sed '1d' stop_times.txt > tempfile; mv tempfile stop_times.txt; cd ../


#concat all files into a single file
cat google_transit_queens/stop_times.txt google_transit_manhattan/stop_times.txt google_transit_bronx/stop_times.txt google_transit_brooklyn/stop_times.txt google_transit_staten_island/stop_times.txt > stop_times.csv

#add a comma at the beginning of each line, because file does not have the autoincrement id field
sed 's/^/,/' stop_times.csv > stop_times_.csv; mv stop_times_.csv stop_times.csv

#update database
mysql -u lfcunha -e 'truncate table `bus`.`stop_times`;'
mysqlimport --ignore-lines=0 --fields-terminated-by=, --local -u lfcunha     bus stop_times.csv
