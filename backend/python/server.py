__author__ = 'luiscunha'

"""#REST API:
#http://lfcunha.nyc/api/stoptimes/stop/308207
#http://lfcunha.nyc/api/start/center/40.781040248654136,-73.95239476259917/dir/0
#http://lfcunha.nyc/api/buses/buses/B63,B61/dir/1

"""
import sys
import os
from os import sep
from tornado import httpserver as s
sys.path.append(os.path.abspath(os.path.dirname(os.path.abspath(__file__))))

from app import *
from app.app import *

connection = db_connection()

application = tornado.web.Application([
    #(r"/api/stoptimes/stop/([0-9]+)", StopTimesHandler, dict(database=connection)),
    (r"/api/stoptimes/stop/([0-9]+)", StopTimesHandler),
    (r"/api/start/center/([a-zA-Z0-9;,.\-]+)/dir/([0,1])", MainHandler),
    (r"/api/routegeometry/route/([a-zA-Z0-9;,.\-\+]+)/direction/([0,1])/center/([a-zA-Z0-9;,.\-\+]+)", RouteGeometry),
    (r"/api/buses/buses/([a-zA-Z0-9;,.\-+]+)/dir/([0,1])", GetBusesLocationsHandler),
    (r"/api/test", Test),
], debug=True)

http_server = s.HTTPServer(application)
http_server.listen(8889, address='127.0.0.1')

#application.listen(8889, address='127.0.0.1')
tornado.ioloop.IOLoop.current().start()

