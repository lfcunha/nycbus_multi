__author__ = 'luiscunha'

from app.bin.download_urls_parallel import *
from app.config.config import *
import json
from tornado import gen

class BusLine:
    def __init__(self, buses, direction=0):
        print("b: ", buses)
        self.buses = buses
        self.base_url = "http://bustime.mta.info/api/siri/vehicle-monitoring.json?key=" + KEY + "&OperatorRef=MTA%20NYCT&LineRef=MTA%20NYCT_"
        self.urls = [self.base_url + bus + "&DirectionRef=" + str(direction) for bus in self.buses]
        print(self.buses)
        print(self.urls)


    #async def download(self):
    #    downloader = Download_parallel(self.urls, 60)
    #    data = await downloader.download
    #    return data

    @gen.coroutine
    def download(self):
        downloader = Download_parallel(self.urls, 60)
        data = yield downloader.download
        raise gen.Return(data)

    @property
    @gen.coroutine
    def run(self):
        result = yield self.download()
        if result:
            master = {}
            for bus in result:
                bus = json.loads(bus)
                for vehicle in bus["Siri"]['ServiceDelivery']['VehicleMonitoringDelivery']:
                    if vehicle.get("VehicleActivity", None) != None:
                        for vehicleActivity in vehicle["VehicleActivity"]:
                            ID = vehicleActivity['MonitoredVehicleJourney']['VehicleRef']
                            master[ID] = {}
                            master[ID]['PublishedLineName'] = vehicleActivity['MonitoredVehicleJourney']["PublishedLineName"]
                            master[ID]['VehicleRef'] = vehicleActivity['MonitoredVehicleJourney']["VehicleRef"]
                            master[ID]['Direction'] = vehicleActivity['MonitoredVehicleJourney']["DirectionRef"]
                            master[ID]['DestinationName'] = vehicleActivity['MonitoredVehicleJourney']["DestinationName"]
                            master[ID]['Longitude'] = vehicleActivity['MonitoredVehicleJourney']["VehicleLocation"]["Longitude"]
                            master[ID]['Latitude']  = vehicleActivity['MonitoredVehicleJourney']["VehicleLocation"]["Latitude"]
                            master[ID]['Bearing'] = vehicleActivity['MonitoredVehicleJourney']["Bearing"]
                            master[ID]['ProgressRate'] = vehicleActivity['MonitoredVehicleJourney']["ProgressRate"]
                            master[ID]['JourneyPatternRef'] = vehicleActivity['MonitoredVehicleJourney']["JourneyPatternRef"]
                            master[ID]['DatedVehicleJourneyRef'] = vehicleActivity['MonitoredVehicleJourney']["FramedVehicleJourneyRef"]["DatedVehicleJourneyRef"]
                            if vehicleActivity['MonitoredVehicleJourney'].get("MonitoredCall", None) != None:
                                master[ID]['StopPointName'] = vehicleActivity['MonitoredVehicleJourney']["MonitoredCall"]["StopPointName"]
                                master[ID]['StopPointRef'] = vehicleActivity['MonitoredVehicleJourney']["MonitoredCall"]["StopPointRef"]
                                master[ID]['PresentableDistance'] = vehicleActivity['MonitoredVehicleJourney']["MonitoredCall"]["Extensions"]["Distances"]["PresentableDistance"]
            raise gen.Return(master)
