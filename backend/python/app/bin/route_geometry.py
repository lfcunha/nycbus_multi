__author__ = 'luiscunha'

import re
import pymysql.cursors  # https://github.com/PyMySQL/PyMySQL
from app.config.config import *

class RouteGeometry:
    def __init__(self, routes, direction, center):
        self.routes = routes.split(",")
        self.direction = direction
        self.center = center
        self.db = pymysql.connect(host=HOST,
                            port=PORT,
                            user=USER,
                            password=PASSWD,
                            db=DB,
                            charset='utf8mb4',
                            cursorclass=pymysql.cursors.DictCursor)
        self.geometries = {}
        self.obj = {}

    def get_routes(self):
        if self.direction == "0": self.direction = ''
        connection = self.db
        query = "SELECT `route_id`, `geometry_` FROM `routes` WHERE ("

        for route in self.routes:
            if route == "S79+": route = "S79"
            if (route == "M15" or route == "M14A" or route == "M14D"): route = "M14AD"

            query += "(`route_id` like '" + route + "'   AND  `geometry_vertex_count` = ( SELECT MAX( `geometry_vertex_count` ) FROM  `routes` WHERE `route_id` = '"+ route + "' AND `direction_`= '"+self.direction+"') AND `direction_`= '"+ self.direction + " ') OR"

        query = query[:-4] + "));"
        try:
            with connection.cursor() as cursor:
                # Create a new record
                cursor.execute(query)
            connection.commit()
            result = cursor.fetchall()
        except:
            pass
        else:
            self.obj["type"] = "FeatureCollection"
            self.obj["features"] = []
            for row in result:
                self.geometries[row["route_id"]] = []
                geo = row["geometry_"]
                geo = re.sub("<LineString><coordinates>", "", geo)
                geo = geo.split()
                coordinates = []
                feature = {}
                feature["type"] = "Feature"
                geometry = {}
                geometry["type"] = "LineString"
                geometry["properties"] = {}
                geometry["properties"]["bus_line"] = row["route_id"]

                for point in geo:
                    p = point.split(",")[:-1]
                    self.geometries[row["route_id"]].append(",".join(p))
                    p = list(map(lambda x: float(x), p))
                    coordinates.append(p)

                geometry["coordinates"] = coordinates
                feature["geometry"] = geometry
                self.obj["features"].append(geometry)

            return self.obj
        finally:
            connection.close()

    @property
    def run(self):
        return self.get_routes()
