__author__ = 'luiscunha'


import concurrent.futures
import urllib.request
import time
from tornado import gen

class Download_parallel:
    def __init__(self, urls, timeout=60):
        self.urls = urls
        self.timeout = timeout

    # Retrieve a single page and report the url and contents

    def load_url(self, url, timeout):
        with urllib.request.urlopen(url, timeout=timeout) as conn:
            return conn.read()

    @property
    @gen.coroutine
    def download(self):
        data1 = []
        start = time.time()
        # We can use a with statement to ensure threads are cleaned up promptly
        with concurrent.futures.ThreadPoolExecutor(max_workers=len(self.urls)) as executor:
            # Start the load operations and mark each future with its URL
            future_to_url = {executor.submit(self.load_url, url, 60): url for url in self.urls}
            for future in concurrent.futures.as_completed(future_to_url):
                url = future_to_url[future]
                try:
                    data = future.result()
                    data1.append(data.decode("utf-8"))  # decode to convert from byte to string
                except Exception as exc:
                    print('%r generated an exception: %s' % (url, exc))
                else:
                    print('%r page is %d bytes' % (url, len(data)))
            print("done")

            endtime = time.time() - start
            print("Finished in {endtime:.3} sec".format(**locals()))
            raise gen.Return(data1)
