__author__ = 'luiscunha'

from app.config.config import *
import pymysql.cursors


class GetStopsWithinRadius:
    """
    Get info for all stops within 0.0034 degrees of location
    return list of stops, plus a list of the set of buses serviced by all these stops
    """

    def __init__(self, center, direction):
        self.center = center
        self.direction = direction
        self.db = pymysql.connect(host=HOST,
                                  port=PORT,
                                  user=USER,
                                  password=PASSWD,
                                  db=DB,
                                  charset='utf8mb4',
                                  cursorclass=pymysql.cursors.DictCursor)

    @property
    def run(self):
        import itertools

        connection = self.db
        query = "SELECT * FROM `stop_times` FORCE INDEX (tripID) WHERE ("

        lat, lon = map(float, self.center.split(","))

        # square box (instead of radius) around center +- 0.0034 degrees
        e = str(lon + 0.0034)
        w = str(lon - 0.0034)
        n = str(lat + 0.0034)
        s = str(lat - 0.0034)

        query = " SELECT * FROM  `stopsDir` WHERE (`lat` <  %s  AND  `lat` > %s AND `lon` > %s AND `lon`  < %s AND" \
                " `dir`= %s)LIMIT 0 , 1000";

        try:
            # with connection.cursor() as cursor:
            # Create a new record
            cursor = connection.cursor()
            sql = query
            cursor.execute(sql, (n, s, w, e, self.direction))
            connection.commit()
            result = cursor.fetchall()
        except Exception as e:
            with open("/home/lfcunha/logs/nycbus_errors", "a") as fh:
                fh.write(__file__ + " - " + "query: " + sql)
                fh.write("\n")
                fh.write(str(e))
                fh.write("\n")
        finally:
            connection.close()
            stops = []
            buses = set()
            print("r: ")
            print(result)
            for res in result:
                arr = dict()
                arr["buses"] = res.get("buses")
                arr["stop"] = res["number"]
                arr["lat"] = float(res["lat"])
                arr["lon"] = float(res["lon"])
                arr["dir"] = res["dir"]
                stops.append(arr)
                buses.add(res["buses"])
            print("s: ", stops)
            buses = [x.split(",") for x in buses]
            buses = list(set(itertools.chain.from_iterable(buses)))
            buses = list(map(lambda x: x.replace("+", "%2b"), buses))
            stops.append(buses)
            print("stops ", stops)
            return stops
