__author__ = 'luiscunha'

import concurrent.futures
import urllib.request
import time, json

KEY = "ef4e5e00-06e5-4bea-b58b-b42a41fa3e0b"


class StopMonitor:
    def __init__(self, stops):
        """

        :rtype : json
        """
        if type(stops) != "str":
            self.stops = str(stops).split(",")
        else:
            self.stops = stops.split(",")
        self.master = {}  # dictionary of stops:buses
        self.url = "http://bustime.mta.info/api/siri/stop-monitoring.json?key=" + KEY + "&MonitoringRef="

    # Retrieve a single page and report the url and contents
    def load_url(self, stop, timeout):
        stop_url = self.url + stop
        with urllib.request.urlopen(stop_url, timeout=timeout) as conn:
            return conn.read()


    from tornado import gen
    from tornado.httpclient import AsyncHTTPClient

    @gen.coroutine
    def fetch_coroutine(self, url):
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(url)
        raise gen.Return(response.body)


    def get_data(self):
        body = self.fetch_coroutine(url)
        data1.append(body)

    def get_data1(self):
        data1 = []
        start = time.time()
        # We can use a with statement to ensure threads are cleaned up promptly
        with concurrent.futures.ThreadPoolExecutor(max_workers=len(self.stops)) as executor:
            # Start the load operations and mark each future with its URL
            future_to_url = {executor.submit(self.load_url, stop, 60): stop for stop in self.stops}
            for future in concurrent.futures.as_completed(future_to_url):
                url = future_to_url[future]
                try:
                    data = future.result()
                    data1.append(data.decode("utf-8"))  # decode to convert from byte to string
                except Exception as exc:
                    print('%r generated an exception: %s' % (url, exc))
                else:
                    print('%r page is %d bytes' % (url, len(data)))
            print("done")

        endtime = time.time() - start
        print("Finished in {endtime:.3} sec".format(**locals()))
        return data1

    def parse_stop_monitor_result(self, data=None):
        """ parse stop_monitor feed's json format for bus information on route to the queried stop
        :param data:
        :return: json
        """
        stop = json.loads(data[0])
        StopMonitoringDelivery = stop["Siri"]["ServiceDelivery"][
            "StopMonitoringDelivery"]  # ["MonitoredStopVisit"])#["MonitoredVehicleJourney"]) #MonitoredCall

        for stop_ in StopMonitoringDelivery:
            try:
                for visit in stop_["MonitoredStopVisit"]:
                    id = visit["MonitoredVehicleJourney"]["FramedVehicleJourneyRef"]["DatedVehicleJourneyRef"][9:]
                    stop = visit["MonitoredVehicleJourney"]["MonitoredCall"]["StopPointRef"][4:]
                    if stop not in self.master: self.master[stop] = {}
                    self.master[stop][id] = {"PublishedLineName": visit["MonitoredVehicleJourney"]["PublishedLineName"],
                                             "ProgressRate": visit["MonitoredVehicleJourney"]["ProgressRate"],
                                             "PresentableDistance":
                                                 visit["MonitoredVehicleJourney"]["MonitoredCall"]["Extensions"][
                                                     "Distances"][
                                                     "PresentableDistance"],
                                             "StopsFromCall":
                                                 visit["MonitoredVehicleJourney"]["MonitoredCall"]["Extensions"][
                                                     "Distances"][
                                                     "StopsFromCall"],
                                             "VehicleRef": visit["MonitoredVehicleJourney"]["VehicleRef"],
                                             "DatedVehicleJourneyRef": id
                                             }
            except KeyError:
                raise

        return self.master

    def run(self):
        return self.parse_stop_monitor_result(self.get_data())





import HTMLParser
import time
import urlparse2
from datetime import timedelta

from tornado import httpclient, gen, ioloop, queues

base_url = 'http://www.tornadoweb.org/en/stable/'
concurrency = 10


@gen.coroutine
def get_links_from_url(url):
    """Download the page at `url` and parse it for links.

    Returned links have had the fragment after `#` removed, and have been made
    absolute so, e.g. the URL 'gen.html#tornado.gen.coroutine' becomes
    'http://www.tornadoweb.org/en/stable/gen.html'.
    """
    try:
        response = yield httpclient.AsyncHTTPClient().fetch(url)
        print('fetched %s' % url)
        urls = [urlparse2.urljoin(url, remove_fragment(new_url))
                for new_url in get_links(response.body)]
    except Exception as e:
        print('Exception: %s %s' % (e, url))
        raise gen.Return([])

    raise gen.Return(urls)


def remove_fragment(url):
    scheme, netloc, url, params, query, fragment = urlparse2.urlparse(url)
    return urlparse2.urlunparse((scheme, netloc, url, params, query, ''))


def get_links(html):
    class URLSeeker(HTMLParser.HTMLParser):
        def __init__(self):
            HTMLParser.HTMLParser.__init__(self)
            self.urls = []

        def handle_starttag(self, tag, attrs):
            href = dict(attrs).get('href')
            if href and tag == 'a':
                self.urls.append(href)

    url_seeker = URLSeeker()
    url_seeker.feed(html)
    return url_seeker.urls


@gen.coroutine
def main():
    q = queues.Queue()
    start = time.time()
    fetching, fetched = set(), set()

    @gen.coroutine
    def fetch_url():
        current_url = yield q.get()
        try:
            if current_url in fetching:
                return

            print('fetching %s' % current_url)
            fetching.add(current_url)
            urls = yield get_links_from_url(current_url)
            fetched.add(current_url)

            for new_url in urls:
                # Only follow links beneath the base URL
                if new_url.startswith(base_url):
                    yield q.put(new_url)

        finally:
            q.task_done()

    @gen.coroutine
    def worker():
        while True:
            yield fetch_url()

    q.put(base_url)

    # Start workers, then wait for the work queue to be empty.
    for _ in range(concurrency):
        worker()
    yield q.join(timeout=timedelta(seconds=300))
    assert fetching == fetched
    print('Done in %d seconds, fetched %s URLs.' % (
        time.time() - start, len(fetched)))


if __name__ == '__main__':
    import logging
    logging.basicConfig()
    io_loop = ioloop.IOLoop.current()
    io_loop.run_sync(main)

if __name__ == "__main__":
    data = [
        '{"Siri":{"ServiceDelivery":{"ResponseTimestamp":"2015-08-09T21:44:37.703-04:00","StopMonitoringDelivery":[{"MonitoredStopVisit":[{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M34A+","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_MQ_C5-Sunday-128200_SBS34_30"},"JourneyPatternRef":"MTA_M34A0023","PublishedLineName":"M34A-SBS","OperatorRef":"MTA NYCT","OriginRef":"MTA_803099","DestinationRef":"MTA_903027","DestinationName":"WATERSIDE VIA 34 ST VIA 2 AV","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-73.97923,"Latitude":40.746194},"Bearing":337.00943,"ProgressRate":"normalProgress","BlockRef":"MTA NYCT_MQ_C5-Sunday_D_MQ_24600_SBS34-8","VehicleRef":"MTA NYCT_5848","MonitoredCall":{"Extensions":{"Distances":{"PresentableDistance":"1 stop away","DistanceFromCall":308.13,"StopsFromCall":1,"CallDistanceAlongRoute":2612.35}},"StopPointRef":"MTA_401827","VisitNumber":1,"StopPointName":"E 34 ST/2 AV"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:13.675-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M34+","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_MQ_C5-Sunday-129700_SBS34_25"},"JourneyPatternRef":"MTA_M340055","PublishedLineName":"M34-SBS","OperatorRef":"MTA NYCT","OriginRef":"MTA_803131","DestinationRef":"MTA_903028","DestinationName":"SELECT BUS SERVICE Eastside - FDR DR","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-73.991139,"Latitude":40.751208},"Bearing":337.10846,"ProgressRate":"normalProgress","BlockRef":"MTA NYCT_MQ_C5-Sunday_D_MQ_21360_SBS34-4","VehicleRef":"MTA NYCT_5857","MonitoredCall":{"Extensions":{"Distances":{"PresentableDistance":"0.9 miles away","DistanceFromCall":1455.97,"StopsFromCall":4,"CallDistanceAlongRoute":2686.23}},"StopPointRef":"MTA_401827","VisitNumber":1,"StopPointName":"E 34 ST/2 AV"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:12.014-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M34A+","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_MQ_C5-Sunday-131200_SBS34_29"},"JourneyPatternRef":"MTA_M34A0023","PublishedLineName":"M34A-SBS","OperatorRef":"MTA NYCT","OriginRef":"MTA_803099","DestinationRef":"MTA_903027","DestinationName":"WATERSIDE VIA 34 ST VIA 2 AV","OriginAimedDepartureTime":"2015-08-09T21:52:00.000-04:00","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-73.990495,"Latitude":40.756268},"Bearing":53.81449,"ProgressRate":"normalProgress","ProgressStatus":"prevTrip","BlockRef":"MTA NYCT_MQ_C5-Sunday_D_MQ_36660_SBS34-13","VehicleRef":"MTA NYCT_5849","MonitoredCall":{"Extensions":{"Distances":{"PresentableDistance":"1.9 miles away","DistanceFromCall":3117.12,"StopsFromCall":11,"CallDistanceAlongRoute":2612.35}},"StopPointRef":"MTA_401827","VisitNumber":1,"StopPointName":"E 34 ST/2 AV"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:12.133-04:00"}],"ResponseTimestamp":"2015-08-09T21:44:37.703-04:00","ValidUntil":"2015-08-09T21:45:37.703-04:00"}],"SituationExchangeDelivery":[{"Situations":{"PtSituationElement":[{"PublicationWindow":{"StartTime":"2015-08-09T08:30:00.000-04:00"},"Severity":"undefined","Summary":"There is no Sunday service on the BM1, BM2, BM3, BM4 and BM5 buses.","Description":"There is no Sunday service on the BM1, BM2, BM3, BM4 and BM5 buses.","Affects":{"VehicleJourneys":{"AffectedVehicleJourney":[{"LineRef":"MTABC_BM1","DirectionRef":"0"},{"LineRef":"MTABC_BM1","DirectionRef":"1"},{"LineRef":"MTABC_BM2","DirectionRef":"0"},{"LineRef":"MTABC_BM2","DirectionRef":"1"},{"LineRef":"MTABC_BM3","DirectionRef":"0"},{"LineRef":"MTABC_BM3","DirectionRef":"1"},{"LineRef":"MTABC_BM4","DirectionRef":"0"},{"LineRef":"MTABC_BM4","DirectionRef":"1"},{"LineRef":"MTABC_BM5","DirectionRef":"0"},{"LineRef":"MTABC_BM5","DirectionRef":"1"}]}},"Consequences":{"Consequence":[{"Condition":"altered"}]},"CreationTime":"2015-08-09T12:02:27.823-04:00","SituationNumber":"MTABC_9c48f7b7-42bb-44b3-a416-3a574ed14bbd"}]}}]}}}',
        '{"Siri":{"ServiceDelivery":{"ResponseTimestamp":"2015-08-09T21:44:37.732-04:00","StopMonitoringDelivery":[{"MonitoredStopVisit":[{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M34A+","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_MQ_C5-Sunday-128200_SBS34_30"},"JourneyPatternRef":"MTA_M34A0023","PublishedLineName":"M34A-SBS","OperatorRef":"MTA NYCT","OriginRef":"MTA_803099","DestinationRef":"MTA_903027","DestinationName":"WATERSIDE VIA 34 ST VIA 2 AV","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-73.97923,"Latitude":40.746194},"Bearing":337.00943,"ProgressRate":"normalProgress","BlockRef":"MTA NYCT_MQ_C5-Sunday_D_MQ_24600_SBS34-8","VehicleRef":"MTA NYCT_5848","MonitoredCall":{"Extensions":{"Distances":{"PresentableDistance":"approaching","DistanceFromCall":152.02,"StopsFromCall":0,"CallDistanceAlongRoute":2456.24}},"StopPointRef":"MTA_401826","VisitNumber":1,"StopPointName":"E 34 ST/3 AV"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:13.675-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M34+","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_MQ_C5-Sunday-129700_SBS34_25"},"JourneyPatternRef":"MTA_M340055","PublishedLineName":"M34-SBS","OperatorRef":"MTA NYCT","OriginRef":"MTA_803131","DestinationRef":"MTA_903028","DestinationName":"SELECT BUS SERVICE Eastside - FDR DR","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-73.991139,"Latitude":40.751208},"Bearing":337.10846,"ProgressRate":"normalProgress","BlockRef":"MTA NYCT_MQ_C5-Sunday_D_MQ_21360_SBS34-4","VehicleRef":"MTA NYCT_5857","MonitoredCall":{"Extensions":{"Distances":{"PresentableDistance":"0.8 miles away","DistanceFromCall":1299.87,"StopsFromCall":3,"CallDistanceAlongRoute":2530.12}},"StopPointRef":"MTA_401826","VisitNumber":1,"StopPointName":"E 34 ST/3 AV"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:12.014-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M34A+","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_MQ_C5-Sunday-131200_SBS34_29"},"JourneyPatternRef":"MTA_M34A0023","PublishedLineName":"M34A-SBS","OperatorRef":"MTA NYCT","OriginRef":"MTA_803099","DestinationRef":"MTA_903027","DestinationName":"WATERSIDE VIA 34 ST VIA 2 AV","OriginAimedDepartureTime":"2015-08-09T21:52:00.000-04:00","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-73.990495,"Latitude":40.756268},"Bearing":53.81449,"ProgressRate":"normalProgress","ProgressStatus":"prevTrip","BlockRef":"MTA NYCT_MQ_C5-Sunday_D_MQ_36660_SBS34-13","VehicleRef":"MTA NYCT_5849","MonitoredCall":{"Extensions":{"Distances":{"PresentableDistance":"1.8 miles away","DistanceFromCall":2961.02,"StopsFromCall":10,"CallDistanceAlongRoute":2456.24}},"StopPointRef":"MTA_401826","VisitNumber":1,"StopPointName":"E 34 ST/3 AV"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:12.133-04:00"}],"ResponseTimestamp":"2015-08-09T21:44:37.732-04:00","ValidUntil":"2015-08-09T21:45:37.732-04:00"}],"SituationExchangeDelivery":[{"Situations":{"PtSituationElement":[{"PublicationWindow":{"StartTime":"2015-08-09T08:30:00.000-04:00"},"Severity":"undefined","Summary":"There is no Sunday service on the BM1, BM2, BM3, BM4 and BM5 buses.","Description":"There is no Sunday service on the BM1, BM2, BM3, BM4 and BM5 buses.","Affects":{"VehicleJourneys":{"AffectedVehicleJourney":[{"LineRef":"MTABC_BM1","DirectionRef":"0"},{"LineRef":"MTABC_BM1","DirectionRef":"1"},{"LineRef":"MTABC_BM2","DirectionRef":"0"},{"LineRef":"MTABC_BM2","DirectionRef":"1"},{"LineRef":"MTABC_BM3","DirectionRef":"0"},{"LineRef":"MTABC_BM3","DirectionRef":"1"},{"LineRef":"MTABC_BM4","DirectionRef":"0"},{"LineRef":"MTABC_BM4","DirectionRef":"1"},{"LineRef":"MTABC_BM5","DirectionRef":"0"},{"LineRef":"MTABC_BM5","DirectionRef":"1"}]}},"Consequences":{"Consequence":[{"Condition":"altered"}]},"CreationTime":"2015-08-09T12:02:27.823-04:00","SituationNumber":"MTABC_9c48f7b7-42bb-44b3-a416-3a574ed14bbd"}]}}]}}}',
        '{"Siri":{"ServiceDelivery":{"ResponseTimestamp":"2015-08-09T21:44:37.743-04:00","StopMonitoringDelivery":[{"MonitoredStopVisit":[{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M103","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_OH_C5-Sunday-127500_M101_100"},"JourneyPatternRef":"MTA_M1030179","PublishedLineName":"M103","OperatorRef":"MTA NYCT","OriginRef":"MTA_903145","DestinationRef":"MTA_903042","DestinationName":"E HARLEM 125 ST via 3 AV","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-73.980204,"Latitude":40.742871},"Bearing":53.774353,"ProgressRate":"normalProgress","BlockRef":"MTA NYCT_OH_C5-Sunday_D_OH_40020_M101-82","VehicleRef":"MTA NYCT_5639","MonitoredCall":{"Extensions":{"Distances":{"StopsFromCall":3,"CallDistanceAlongRoute":5084.72,"DistanceFromCall":654.34,"PresentableDistance":"3 stops away"}},"StopPointRef":"MTA_402677","VisitNumber":1,"StopPointName":"3 AV/E 37 ST"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:23.000-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M102","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_OH_C5-Sunday-129800_M101_99"},"JourneyPatternRef":"MTA_M1020120","PublishedLineName":"M102","OperatorRef":"MTA NYCT","OriginRef":"MTA_903147","DestinationRef":"MTA_803024","DestinationName":"HARLEM 147 ST via 3 AV","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-73.980905,"Latitude":40.741907},"Bearing":53.950687,"ProgressRate":"normalProgress","BlockRef":"MTA NYCT_OH_C5-Sunday_D_OH_22860_M101-16","VehicleRef":"MTA NYCT_5538","MonitoredCall":{"Extensions":{"Distances":{"StopsFromCall":3,"CallDistanceAlongRoute":2356.29,"DistanceFromCall":776.72,"PresentableDistance":"3 stops away"}},"StopPointRef":"MTA_402677","VisitNumber":1,"StopPointName":"3 AV/E 37 ST"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:11.000-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M101","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_OH_C5-Sunday-129100_M101_145"},"JourneyPatternRef":"MTA_M1010343","PublishedLineName":"M101","OperatorRef":"MTA NYCT","OriginRef":"MTA_903147","DestinationRef":"MTA_803020","DestinationName":"FT GEORGE 193 ST via 3 AV","OriginAimedDepartureTime":"2015-08-09T21:31:00.000-04:00","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-73.990213,"Latitude":40.729128},"Bearing":233.90515,"ProgressRate":"normalProgress","ProgressStatus":"prevTrip","BlockRef":"MTA NYCT_OH_C5-Sunday_D_OH_26400_M101-26","VehicleRef":"MTA NYCT_5885","MonitoredCall":{"Extensions":{"Distances":{"StopsFromCall":12,"CallDistanceAlongRoute":2356.29,"DistanceFromCall":2419.72,"PresentableDistance":"1.5 miles away"}},"StopPointRef":"MTA_402677","VisitNumber":1,"StopPointName":"3 AV/E 37 ST"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:30.000-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M103","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_OH_C5-Sunday-129000_M101_103"},"JourneyPatternRef":"MTA_M1030179","PublishedLineName":"M103","OperatorRef":"MTA NYCT","OriginRef":"MTA_903145","DestinationRef":"MTA_903042","DestinationName":"E HARLEM 125 ST via 3 AV","OriginAimedDepartureTime":"2015-08-09T21:30:00.000-04:00","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-74.007087,"Latitude":40.711772},"Bearing":166.17795,"ProgressRate":"noProgress","ProgressStatus":"layover,prevTrip","BlockRef":"MTA NYCT_OH_C5-Sunday_D_OH_24840_M101-20","VehicleRef":"MTA NYCT_5633","MonitoredCall":{"Extensions":{"Distances":{"StopsFromCall":21,"CallDistanceAlongRoute":5084.72,"DistanceFromCall":5138.3,"PresentableDistance":"3.2 miles away"}},"StopPointRef":"MTA_402677","VisitNumber":1,"StopPointName":"3 AV/E 37 ST"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:24.000-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M102","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_OH_C5-Sunday-131800_M101_101"},"JourneyPatternRef":"MTA_M1020120","PublishedLineName":"M102","OperatorRef":"MTA NYCT","OriginRef":"MTA_903147","DestinationRef":"MTA_803024","DestinationName":"HARLEM 147 ST via 3 AV","OriginAimedDepartureTime":"2015-08-09T21:58:00.000-04:00","SituationRef":[{"SituationSimpleRef":"MTA NYCT_104562"}],"Monitored":true,"VehicleLocation":{"Longitude":-73.968524,"Latitude":40.761763},"Bearing":233.98662,"ProgressRate":"normalProgress","ProgressStatus":"prevTrip","BlockRef":"MTA NYCT_OH_C5-Sunday_D_OH_37320_M101-80","VehicleRef":"MTA NYCT_5608","MonitoredCall":{"Extensions":{"Distances":{"StopsFromCall":31,"CallDistanceAlongRoute":2356.29,"DistanceFromCall":6633.43,"PresentableDistance":"4.1 miles away"}},"StopPointRef":"MTA_402677","VisitNumber":1,"StopPointName":"3 AV/E 37 ST"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:19.000-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M103","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_OH_C5-Sunday-131000_M101_126"},"JourneyPatternRef":"MTA_M1030178","PublishedLineName":"M103","OperatorRef":"MTA NYCT","OriginRef":"MTA_903145","DestinationRef":"MTA_803153","DestinationName":"E HARLEM 125 ST via 3 AV","OriginAimedDepartureTime":"2015-08-09T21:50:00.000-04:00","SituationRef":[],"Monitored":true,"VehicleLocation":{"Longitude":-73.987481,"Latitude":40.732878},"Bearing":233.8288,"ProgressRate":"normalProgress","ProgressStatus":"prevTrip","BlockRef":"MTA NYCT_OH_C5-Sunday_D_OH_25920_M101-22","VehicleRef":"MTA NYCT_5881","MonitoredCall":{"Extensions":{"Distances":{"StopsFromCall":32,"CallDistanceAlongRoute":5084.72,"DistanceFromCall":8302.15,"PresentableDistance":"5.2 miles away"}},"StopPointRef":"MTA_402677","VisitNumber":1,"StopPointName":"3 AV/E 37 ST"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:31.000-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M101","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_OH_C5-Sunday-133100_M101_113"},"JourneyPatternRef":"MTA_M1010343","PublishedLineName":"M101","OperatorRef":"MTA NYCT","OriginRef":"MTA_903147","DestinationRef":"MTA_803020","DestinationName":"FT GEORGE 193 ST via 3 AV","OriginAimedDepartureTime":"2015-08-09T21:00:00.000-04:00","SituationRef":[{"SituationSimpleRef":"MTA NYCT_104562"}],"Monitored":true,"VehicleLocation":{"Longitude":-73.95698,"Latitude":40.777586},"Bearing":233.75397,"ProgressRate":"normalProgress","ProgressStatus":"prevTrip","BlockRef":"MTA NYCT_OH_C5-Sunday_D_OH_26100_M101-25","VehicleRef":"MTA NYCT_5721","MonitoredCall":{"Extensions":{"Distances":{"StopsFromCall":41,"CallDistanceAlongRoute":2356.29,"DistanceFromCall":8643.57,"PresentableDistance":"5.4 miles away"}},"StopPointRef":"MTA_402677","VisitNumber":1,"StopPointName":"3 AV/E 37 ST"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:21.069-04:00"},{"MonitoredVehicleJourney":{"LineRef":"MTA NYCT_M101","DirectionRef":"0","FramedVehicleJourneyRef":{"DataFrameRef":"2015-08-09","DatedVehicleJourneyRef":"MTA NYCT_OH_C5-Sunday-135100_M101_116"},"JourneyPatternRef":"MTA_M1010343","PublishedLineName":"M101","OperatorRef":"MTA NYCT","OriginRef":"MTA_903147","DestinationRef":"MTA_803020","DestinationName":"FT GEORGE 193 ST via 3 AV","OriginAimedDepartureTime":"2015-08-09T21:20:00.000-04:00","SituationRef":[{"SituationSimpleRef":"MTA NYCT_104562"}],"Monitored":true,"VehicleLocation":{"Longitude":-73.949844,"Latitude":40.787361},"Bearing":233.8549,"ProgressRate":"normalProgress","ProgressStatus":"prevTrip","BlockRef":"MTA NYCT_OH_C5-Sunday_D_OH_19500_M101-9","VehicleRef":"MTA NYCT_5643","MonitoredCall":{"Extensions":{"Distances":{"StopsFromCall":47,"CallDistanceAlongRoute":2356.29,"DistanceFromCall":9885.54,"PresentableDistance":"6.1 miles away"}},"StopPointRef":"MTA_402677","VisitNumber":1,"StopPointName":"3 AV/E 37 ST"},"OnwardCalls":{}},"RecordedAtTime":"2015-08-09T21:44:14.000-04:00"}],"ResponseTimestamp":"2015-08-09T21:44:37.743-04:00","ValidUntil":"2015-08-09T21:45:37.743-04:00"}],"SituationExchangeDelivery":[{"Situations":{"PtSituationElement":[{"PublicationWindow":{"StartTime":"2015-08-07T00:00:00.000-04:00","EndTime":"2016-01-29T23:59:00.000-05:00"},"Severity":"undefined","Summary":"M102 and M116 - Southbound/Westbound bus stop on 116 St at Madison Ave temporarily closed","Description":"M102 and M116 - Southbound/Westbound bus stop on 116 St at Madison Ave temporarily closed \\nUntil further notice\\n\\nDue to construction, please follow directional signage to nearby stops along 116 St.\\n\\nReminder: M116 buses do not operate overnight.","Affects":{"VehicleJourneys":{"AffectedVehicleJourney":[{"LineRef":"MTA NYCT_M102","DirectionRef":"1"},{"LineRef":"MTA NYCT_M116","DirectionRef":"1"}]}},"Consequences":{"Consequence":[{"Condition":"diverted"}]},"CreationTime":"2015-08-07T00:00:00.000-04:00","SituationNumber":"MTA NYCT_104562"}]}}]}}}']

    stops = "402677,401827,401826"
    stopMonitor = StopMonitor("400798")
    try:
        result = stopMonitor.run()
        print(result)
    except KeyError:
        print("oops")

