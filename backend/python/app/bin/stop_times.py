__author__ = "Luis Cunha"

import time as tm
from datetime import date, datetime, time, timedelta

import pymysql.cursors  # https://github.com/PyMySQL/PyMySQL
import tornado_mysql
from app.bin.stop_monitor import StopMonitor as Stops
from app.config.config import *
from tornado import gen

class StopTimes:
    """Get a list of arrival times (min to) for all lines served by the argument stop

    """
    def __init__(self, stop):
        self.stop = stop
        """
        self.db = pymysql.connect(host=HOST,
                            port=PORT,
                            user=USER,
                            password=PASSWD,
                            db=DB,
                            charset='utf8mb4',
                            cursorclass=pymysql.cursors.DictCursor)
        """


    @gen.coroutine
    def times(self):
        conn = yield tornado_mysql.connect(host=HOST, port=PORT, user=USER, passwd=PASSWD, db=DB)
        cur = conn.cursor()
        """Gets list of buses on route to this stop; Queries their schedule from the database and calculate
        time of arrival at query stop, adjusted for current tardiness to its next stop

        :return: dict arrivals
        """

        try:
            stops = Stops(self.stop)
            print("\n###### GETTING BUSES ON ROUTE TO STOP: " + str(self.stop) + " ##########")
            data = yield stops.run()  # List of buses on route to this stop
            print("d: ", data)
            if len(data[self.stop]) == 0:
                raise gen.Return(None)
            print("buses on route: " + str(data))
        except KeyError:
            print("oops, no such stop name")
        except Exception as e:
            print("error", e)
        else:
            schedule = {}  # schedule for all buses on route - the database entries for every stop of each trip
            master = {}  # keep data about the buses' trip
            arrivals = {}  # array of bus_line:[arrivals in x min] to be returned to caller
            line_buses = {}  # keep track of line name (e.g. B63) for each Bus trip. We'll need it later
            stopsfromcall = {}  # stops from call for each bus, that is, stops away from the query stop
            print("\n######### PROCESS BUS INFO #############")
            for bus in data[self.stop]:
                print("bus on route: " + bus)
                schedule[bus] = {}
                stopsFromCall = data[self.stop][bus]["StopsFromCall"]
                if data[self.stop][bus]["PublishedLineName"] not in master:
                    master[data[self.stop][bus]["PublishedLineName"]] = {}
                master[data[self.stop][bus]["PublishedLineName"]][bus] = {
                    "ProgressRate": data[self.stop][bus]["ProgressRate"],
                    "StopsFromCall": data[self.stop][bus]["StopsFromCall"],
                    "VehicleRef": data[self.stop][bus]["VehicleRef"],
                    "DatedVehicleJourneyRef": data[self.stop][bus]["DatedVehicleJourneyRef"]
                }
                stopsfromcall[bus] = data[self.stop][bus]["StopsFromCall"]
            print("master information: " + str(master))
            query = "SELECT * FROM `stop_times` FORCE INDEX (tripID) WHERE ("

            for line in list(master):
                for bus in list(master[line]):
                    line_buses[bus] = line
                    query += " `trip_id` LIKE '" + bus + "' OR "

            query = query[:-3] + ");"
            print("query the schedule db: " + str(query))
            # Connect to the database

            try:
                yield cur.execute(query)
                print(cur.description)
                result_ = cur.fetchall()
                col = [x[0] for x in cur.description]
                result = [dict(zip(col, x)) for x in result_]

            except Exception as e:
                print("Error querying db in stop_times", e)
            finally:
                conn.close()
                for res in result:
                    if res["trip_id"] not in schedule:
                        schedule[res["trip_id"]] = {}
                    try:
                        dept_time = str(res["departure_time"]).split(":")
                        dept_time = list(map(int, dept_time))
                        dt = datetime.combine(date.today(), time(dept_time[0], dept_time[1], dept_time[2]))
                    except:
                        # Bus trips that start before midnight, once arrival passes midnight, it continues into hours 24, 25, ...
                        dept_time = res["departure_time"] - timedelta(days=1)
                        dept_time = str(dept_time).split(":")
                        dept_time = list(map(int, dept_time))
                        dt = datetime.combine(date.today(), time(dept_time[0], dept_time[1], dept_time[2])) + timedelta(
                            days=1)

                    schedule[res["trip_id"]][res["stop_sequence"]] = {"stop_id": res["stop_id"],
                                                                      "departure_time": dt
                                                                      }
                    if str(res["stop_id"]) == self.stop:
                        schedule[res["trip_id"]]["my_stop"] = res["stop_sequence"]
                        schedule[res["trip_id"]]["departure_time"] = dt

                print("all stops by all buses: " + str(result))
                print("schedule: " + str(schedule))
                print("\n############### PROCESS THE SCHEDULE INFO ###############")
                now = tm.time()
                for bus in schedule:
                    line = line_buses[bus]
                    if line not in arrivals:
                        arrivals[line] = []
                    print("\n****\nbus: " + bus)
                    try:
                        my_stop = schedule[bus]["my_stop"]  # the clicked stop
                    except:
                        print("\n****\nschedule: ")
                        print(schedule)
                        print("\n****\n ")
                    stops_from_Call = stopsfromcall[bus]  # number of stops away from the clicked stop
                    # print(schedule[bus])
                    m = max(list(filter(lambda x_: isinstance(x_, int), list(schedule[bus]))))  # number of stops

                    current_stop = my_stop - stops_from_Call  # stop number of the current stop
                    # the # of stops away could be higher than number of stops when the bus is going to turn around
                    # ignore those cases
                    if current_stop > 0:
                        print("current stop: " + str(current_stop) + " ; mystop: " + str(
                            my_stop) + " ;stops from call: " + str(stops_from_Call) + " ;number of stops: " + str(m))
                        print("current stop scheduled: " + str(schedule[bus][current_stop]))
                        late = datetime.now() - schedule[bus][current_stop]["departure_time"]
                        if late.days < 0:
                            late = datetime.now() - schedule[bus][current_stop]["departure_time"]
                        print("scheduled current: " + str(schedule[bus][current_stop]["departure_time"]))
                        print("late: " + str(late.seconds) + " seconds")
                        print("scheduled my_Stop: ", my_stop, schedule[bus][my_stop]["departure_time"])
                        arrival_time = schedule[bus][my_stop]["departure_time"] + late
                        print("arrival time my stop: " + str(arrival_time))
                        arriving_in = str(int(((arrival_time - datetime.now()).seconds) / 60))
                        if str(arriving_in) == "1439": #when arriving_in result is -1 day
                            arriving_in = "0"
                        print("a123", arrival_time, datetime.now(), arrival_time - datetime.now())
                        print("arriving my stop in: " + arriving_in)
                        arrivals[line].append(arriving_in)
                for line in arrivals:
                    arrivals[line].sort(key=lambda x: int(x))
                    filter(lambda x: x < 180, arrivals[line])
                print("Processing took: " + str(tm.time() - now) + " sec")
            print(arrivals)
            raise gen.Return(arrivals)

    @gen.coroutine
    def run(self):
        print("run")
        raise gen.Return(self.times())


if __name__ == "__main__":
    import json
    import tornado.ioloop
    import tornado.web

    class MainHandler(tornado.web.RequestHandler):
        def initialize(self, database):
            self.db = database

        def get(self, stop):
            self.add_header("Access-Control-Allow-Origin", "*")
            try:
                t = StopTimes(stop)
            except KeyError:
                print("no such stop name")
            # print(t.run())
            else:
                arrivals = t.run()
                self.write(json.dumps(arrivals))

    application = tornado.web.Application([
        (r"/stoptimes/stop/([0-9]+)", MainHandler),
    ])

    if __name__ == "__main__":
        application.listen(8888)
        tornado.ioloop.IOLoop.current().start()
