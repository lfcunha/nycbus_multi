import concurrent.futures
import urllib.request
import time, json

url = 'bustime.mta.info/api/siri/stop-monitoring.json?key=ef4e5e00-06e5-4bea-b58b-b42a41fa3e0b&MonitoringRef='
stops = ["400798", "400798", "400798", "400798", "400798", "400798", "400798", "400798", "400798", "400798"]

# Retrieve a single page and report the url and contents
def load_url(stop, timeout):
    stop_url = url + stop
    with urllib.request.urlopen(stop_url, timeout=timeout) as conn:
        return conn.read()

def get_data():
    data1 = []
    start = time.time()
    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ThreadPoolExecutor(max_workers=len(stops)) as executor:
        # Start the load operations and mark each future with its URL
        future_to_url = {executor.submit(load_url, stop, 60): stop for stop in stops}
        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
                data1.append(data.decode("utf-8"))  # decode to convert from byte to string
            except Exception as exc:
                print('%r generated an exception: %s' % (url, exc))
            else:
                print('%r page is %d bytes' % (url, len(data)))
        print("done")

    endtime = time.time() - start
    print("Finished in {endtime:.3} sec".format(**locals()))
    return data1


now  = time.time()
get_data()
print(now - time.time())
