<?php

require_once 'vendor/autoload.php';
use transit_realtime\FeedMessage;
$data = file_get_contents("http://datamine.mta.info/files/k38dkwh992dk/gtfs");
$feed = new FeedMessage();
$feed->parse($data);
foreach ($feed->getEntityList() as $entity) {
  if ($entity->hasTripUpdate()) {
    error_log("trip: " . $entity->getId());
  }
}
