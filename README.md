# README #

NYCBUS.US WEBSITE CODE

### What is in this repository ###

* Frontend single page application, written in raw javascript (+JQuery)
* Backend api code
* Scripts folder contains script to update schedule: download new data, transform, and insert it into database

Backend uses the tornado framework and webserver. MTA network requests are performed in parallel.

Use a NGINX server to deliver the static frontend, and upgrade the connection to the python application running on localhost

An ansible playbook will be added in the future for automated deployment.

### How can I use the API? ###

* The Api provides the following endpoints:
* http://lfcunha.nyc/api/stoptimes/stop/<stop #>      get the schedule for the buses on route to this stop
* http://lfcunha.nyc/api/start/center/<##.##, ##.##>/dir/<0|1>     use the user location to retrieve all stops within a radius, buses on route to those stops, list of bus line names, geometry of the routes (points) and colors of the lines
* http://lfcunha.nyc/api/buses/buses/<comma separated list of buses>/dir/<0|1>    return a list of bus objects for the requested lines
* http://lfcunha.nyc/api/routegeometry/route/<comma separated list of buses>/direction/<0|1>/center/<yes|no>/dir/<0|1>    return list of points to draw these lines



#EXAMPLES


* http://lfcunha.nyc/api/stoptimes/stop/308207
* http://lfcunha.nyc/api/start/center/40.781040248654136,-73.95239476259917/dir/0
* http://lfcunha.nyc/api/buses/buses/B63,B61/dir/1
* http://lfcunha.nyc/api/routegeometry/route/B63/direction/1/center/yes

### OUTPUT ###

* start:  {"busline": [bus objects], "stops": stops, "buses": [list of buses], "route_geometry": route_geometry, "colors": colors}
* 
*