<?
include 'connection.php';
?>




<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map-canvas { height: 380px; width: 300px; }
    </style>


<script type="text/javascript" charset="utf-8" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
<script type="text/javascript" charset="utf-8" src="http://documentcloud.github.io/underscore/underscore-min.js"></script>
<script type="text/javascript" charset="utf-8" src="bus.js"></script>

<script type="text/javascript" charset="utf-8" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>

<link rel="stylesheet" href="styling.css" type="text/css" media=only screen and (max-width: 480px)/>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="default" />

<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width; height=device-height; initial-scale=1.0; maximum-scale=1.0, user-scalable=no " />

<link rel="apple-touch-icon"      href="./bus.png" />
<link rel="apple-touch-startup-image"      href="./splash.png" />

<link href="/maps/documentation/javascript/examples/default.css" rel="stylesheet">
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJPtYQQYO8VRK8FAVWDLZ8R0glGsT21aA&sensor=false"></script>




<script>
function hideAddressBar()
{
  if(!window.location.hash)
  {
      if(document.height < window.outerHeight)
      {
          document.body.style.height = (window.outerHeight + 50) + 'px';
      }

      setTimeout( function(){ window.scrollTo(0, 1); }, 50 );
  }
}

window.addEventListener("load", function(){ if(!window.pageYOffset){ hideAddressBar(); } } );
window.addEventListener("orientationchange", hideAddressBar );



function fixInfoWindow() {
    var proto = google.maps.InfoWindow.prototype,
        open = proto.open;
    proto.open = function(map, anchor, please) {
        if (please) {
            return open.apply(this, arguments);
        }
    }
}


var map;

function initialize() {
  var mapOptions = {
    zoom: 16,
    //disableDefaultUI: true,
    streetViewControl: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  //var mgr = new MarkerManager(map);

  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);



      document.getElementById("center").innerHTML =  "center: " + pos.jb + ", " + pos.kb;


      map.setCenter(pos);
    }, function() {
      handleNoGeolocation(true);
    });

  
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }


  //google.maps.event.addListener(map, 'center_changed', function() {
      google.maps.event.addListener(map, 'click', function() {

       //   window.setTimeout(function() {
      //stops();
   // }, 3000);
          if(map.getZoom()>16) stops();
      }



  );
      google.maps.event.addListener(map, 'zoom_changed', function(){
  zoom=map.getZoom();
  if(zoom<17){
    if(stopsArray){
    for(i in stopsArray){
      stopsArray[i].setMap(null);
    }
  //  stopsArray.length=0;

  }
  }
else{
      if(stopsArray){
    for(i in stopsArray){
      stopsArray[i].setMap(map);
    }
}}

});
}//end intitialize

function array_unique(arr) {
    var result = [];
    for (var i = 0; i < arr.length; i++) {
        if (result.indexOf(arr[i]) == -1) {
            result.push(arr[i]);
        }
    }
    return result;
}

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(40.714353, -74.00597299999998),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position);
}




fixInfoWindow();
google.maps.event.addDomListener(window, 'load', initialize);


var items = [];
 
 //var stopsArray=[];
//////////

function zoomchange(){
  
  var zoon=map.getZoom();
  if (zoon<17){
  

     if(stopsArray){
    for(i in stopsArray){
      stopsArray[i].setMap(null);
    } 
}}};

var stopsArray=[];
function stops(){

function clearStops(){
    if(stopsArray){
    for(i in stopsArray){
      stopsArray[i].setMap(null);
    }
  //  stopsArray.length=0;

  }
    }

function showStops(){
  if(stopsArray){
    //alert(stopsArray.length);

    for(i in stopsArray){

      stopsArray[i].setMap(map);
    }
  }}

//clearStops();

  //alert(items.toString());
  //alert("stops called");  
var bounds = map.getBounds();
console.log("bounds: " + bounds.getNorthEast());
var ne = bounds.getNorthEast(); // LatLng of the north-east corner
var sw = bounds.getSouthWest(); // LatLng of the south-west corder
var nw = new google.maps.LatLng(ne.lat(), sw.lng());
var se = new google.maps.LatLng(sw.lat(), ne.lng());
document.getElementById("bounds").innerHTML = "corners: " + "nw: " + nw + ",ne: " + ne + ",sw: " + sw + ",se: " + se;   

var cenlat=(ne.lat()+se.lat())/2;
var cenlon=(ne.lng()+nw.lng())/2;
var cen=cenlat +"," + cenlon;

document.getElementById("center").innerHTML =  "center: " + cen;
  
function addStops(){

  $.getJSON('getstops3.php?center=' + cen, function(data) {
  //    $.getJSON('getstops.php?test=B16', function(data) {
        items = [];
         $.each(data["buslist"], function(key, val) {
        items.push(val);
        console.log(val);
          });

        $.each(data, function(index) {
            //alert(data[index].lat);
            //console.log(data[index].buses);
            //console.log(data[index].lat);
            //console.log(data[index].lon);
            //console.log(data[index].stop);
            //alert(data[index].lon);
             


console.log(data["buslist"]);

 var zoom=map.getZoom();
 if (zoom>15){
             
   var stop = new google.maps.Circle({
  center:new google.maps.LatLng(data[index].lat,data[index].lon),
  radius:5.5,
  strokeColor:"#ff0000",
  strokeOpacity:0.8,
  strokeWeight:4,
  fillColor:"#0000FF",
  fillOpacity:0.1
  });

//mgr.addMarkers(stop,17);

stopsArray.push(stop);}


        });
    }); //end jsonb
}//end addStops


//mgr.refresh();

addStops();


if(map.getZoom()>15){
  showStops();
}

}//end stops







 ////////////////////////
 
 var markersArray = [];
 var tmp=[];
 var count=0;


function test(buses2){

  var buses=items.toString();
  //alert(buses);
  if(count<2){
stops();

}
    $.ajax({
    type: "GET",
    url: "getsinglebus3.php?bus2="+buses,
    //dataType: 'json',
 
    cache:   false,
    success: function (data) {
    

    
    var b = data.split("*");
    //console.log(b);
    for (a in b){
        tmp[a]=b[a].split(",");
        //console.log(tmp);
    
        }
            
            
   function addMarker(LatLng) {
       marker = new google.maps.Marker({
       position: LatLng,
       map: map
            });
       markersArray.push(marker);}
      
   function markers(){
        for(a in tmp){       
        var LatLng = new google.maps.LatLng(tmp[a][5],tmp[a][4]);  
        addMarker(LatLng);}}

      

            
      
            
                  
 // Removes the overlays from the map, but keeps them in the array
function clearOverlays() {
  if (markersArray) {

    for (i in markersArray) {

      markersArray[i].setMap(null);
       }

    }
            markersArray.length=0;
            //markersArray = [];
  }


// Shows any overlays currently in the array
function showOverlays() {
  if (markersArray) {
    for (i in markersArray) {

              
      markersArray[i].setMap(map);
     // markersArray[i].setMap(map);
    
  }}}
  
          
           markersArray.splice(0, 1);
          
           clearOverlays();                  
           markers();
           showOverlays();
                                          



//could update coordinates everytime viewport changes. however, no need, since this is inside the test function that is called every 10 seconds, so that's the max delay.
//google.maps.event.addListener(map, 'idle', function(ev){
    // update the coordinates here
//});                                                                                   
            
       //document.getElementById("A").innerHTML =  tmp[5];
           document.getElementById("A").innerHTML =  count;
            count++;

//wrapper function to get list of stops from a php script that connets to mysql
//var stops=$.get('http://www.lcfilipe.net/map/getstops.php?center=' + cen);

    




//get boundaries of stops who are between ne/se lat, and nw/ne long. use a class to return an array of the points to mark down, with additional buses at the stop to mark in poppup
var stop = new google.maps.Circle({
  center:new google.maps.LatLng(40.665348,-73.98961),
  radius:0.5,
  strokeColor:"#ff0000",
  strokeOpacity:0.3,
  strokeWeight:5,
  fillColor:"#0000FF",
  fillOpacity:0.1
  });
stop.setMap(map);


          }}) 
          
               
    }//end test
var buses="B63,B61"
setInterval(function() {
    test(buses);
    
}, 5000);

</script>
</head>
<body>



<div id="map-canvas"></div>
<div id="A">count</div>
<div id="center">center</div>
<div id="bounds">bounds</div>
</body>
</html>
