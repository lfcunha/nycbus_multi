var AZHU=new Object();
var offset=1000*60*60*4;
AZHU.storage = {
	save : function(key, jsonData, expirationMin){
		if (!Modernizr.localstorage){return false;}
		var expirationMS = expirationMin * 60 * 1000;

		var record = {value: JSON.stringify(jsonData), timestamp: new Date().getTime() - offset + expirationMS}
		localStorage.setItem(key, JSON.stringify(record));
		return jsonData;
	},
	load : function(key){
		if (!Modernizr.localstorage){return false;}
		var record = JSON.parse(localStorage.getItem(key));
		if (!record){return false;}
		return (new Date().getTime() - offset < record.timestamp && JSON.parse(record.value));
	}
}