<?php

// function that returns an array with fields Direction, Longitude, latitude

function process($bus){

	$json = file_get_contents("http://bustime.mta.info/api/siri/vehicle-monitoring.json?key=ef4e5e00-06e5-4bea-b58b-b42a41fa3e0b&OperatorRef=MTA%20NYCT&LineRef=MTA%20NYCT_".$bus);


//$decode=json_decode($json, true);


//echo "<pre>";

$jsonIterator = new RecursiveIteratorIterator(
    new RecursiveArrayIterator(json_decode($json, TRUE)),
    RecursiveIteratorIterator::SELF_FIRST);

$arr=array();
$master=array();

foreach ($jsonIterator as $key => $val) {

     if ($key=='DirectionRef'){
	
	array_push($arr, $val);
	}}


array_shift($arr);
array_shift($arr);

//array_push($master, $arr);
$master['direction']=array($arr);

//==========
$arr=array();
foreach ($jsonIterator as $key => $val) {

     if ($key=='Longitude'){
	
	array_push($arr, $val);
	}}


array_shift($arr);
array_shift($arr);

//array_push($master, $arr);
$master['Longitude']=array($arr);

$arr=array();
foreach ($jsonIterator as $key => $val) {

     if ($key=='Latitude'){
	
	array_push($arr, $val);
	}}


array_shift($arr);
array_shift($arr);
//array_push($master, $arr);
$master['Latitude']=array($arr);

//=============

return $master;
}  					 //end function process()






//var_dump($master);
												#echo "============\n";
$master=process("B63"); //get data array for B63
$number_buses=count($master[direction][0]);
												#echo $number_buses;
$buses=array();

for($i=0; $i<$number_buses; $i++){
$buses["bus$i"]=array('direction'=>$master['direction'][0][$i], 'Longitude'=>$master['Longitude'][0][$i], 'Latitude'=>$master['Latitude'][0][$i]) ;
}
												#var_dump($buses);
												#echo "=========\n";

for($i=0; $i<$number_buses; $i++){

$B["$i"]=sprintf("%s, %s", $buses["bus$i"]["Latitude"], $buses["bus$i"]["Longitude"]); 
												//$B2=sprintf("%s, %s", $buses["bus2"]["Latitude"], $buses["bus2"]["Longitude"]); 
												//$B3=sprintf("%s, %s", $buses["bus3"]["Latitude"], $buses["bus3"]["Longitude"]); 
												//$B4=sprintf("%s, %s", $buses["bus4"]["Latitude"], $buses["bus4"]["Longitude"]); 

												//echo $B["$i"];
												//echo "\n";
}


 
 for($i=0; $i<$number_buses; $i++){
		if($buses["bus$i"]["direction"]==0)
 			$d["$i"]= 'Pier6';
 		else 
			$d["$i"]= 'Bay Ridge';
//echo $d["$i"], "\n";
}
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.3.1/leaflet.css" />
<!--[if lte IE 8]>
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.3.1/leaflet.ie.css" />
<![endif]-->


<script type="text/javascript" charset="utf-8" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.3/jquery.min.js"></script>
<script src="http://cdn.leafletjs.com/leaflet-0.3.1/leaflet.js"></script>

<link rel="stylesheet" href="styling.css" type="text/css" media=only screen and (max-width: 480px)/>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="default" />



<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width; height=device-height; initial-scale=1.0; maximum-scale=1.0, user-scalable=no " />

<link rel="apple-touch-icon"      href="./bus.png" />
<link rel="apple-touch-startup-image"      href="./splash.png" />




</head>
<body>


<script>
function hideAddressBar()
{
  if(!window.location.hash)
  {
      if(document.height < window.outerHeight)
      {
          document.body.style.height = (window.outerHeight + 50) + 'px';
      }

      setTimeout( function(){ window.scrollTo(0, 1); }, 50 );
  }
}

window.addEventListener("load", function(){ if(!window.pageYOffset){ hideAddressBar(); } } );
window.addEventListener("orientationchange", hideAddressBar );

</script>

<div id="map" style="height:380px; width:300px"></div>
<script type="text/javascript">




var map = new L.Map('map');
var cloudmade = new L.TileLayer('http://{s}.tile.cloudmade.com/9974ede60800436e87bab70b8d73d1e7/997/256/{z}/{x}/{y}.png', {
    attribution: '',
    zoom:'10',
    maxZoom: '16',

});
//CENTER

//++++++++++++++++++++++++++THIS+++++++++++++++++++++++++++
//var home = new L.LatLng(<?php echo $B["0"];?>); // geographical point (longitude and latitude)
//map.setView(home, 12).addLayer(cloudmade);



//++++++++++++++++++++++OR THIS +++++++++++++++++++++++++++++++++



		map.on('locationfound', onLocationFound);
		map.on('locationerror', onLocationError);

		map.locateAndSetView();

		function onLocationFound(e) {
			var radius = e.accuracy / 2;

			var circle = new L.Circle(e.latlng, radius);
			map.addLayer(circle);
		}

		function onLocationError(e) {
			alert(e.message);
		}


map.addLayer(cloudmade);

//++++++++++++++++++++++END LOCATION CENTER CHOICE++++++++++++++++++++++++++++



//MARKERS



<?php for($i=0; $i<$number_buses; $i++){?>

var markerLocation<?php echo $i?> = new L.LatLng(<?php echo $B["$i"];?>);

var marker<?php echo $i?> = new L.Marker(markerLocation<?php echo $i?>);
map.addLayer(marker<?php echo $i?>);
marker<?php echo $i?>.bindPopup("<b>Bus<?php echo $i?> to <?php echo $d["$i"]?>").openPopup();




<?php 
}?>




</script>


</body>
</html>


