<?
function __autoload($class)      //this will load the required class only when necessary (when the class get's called)
{
include_once("class.{$class}.php");
}

class getAllBuses
{
private $master;
private $bus;
private $buses;
private $Lines;

private function run(){

$this->Lines= new lines();    
$this->buses=$this->Lines->get_busInfo(); //requests list of bus lines in service  (eg, M34, B63, etc)




//var_dump($buses);

//for($i=0; $i<count($buses); $i++){  //use instead the simpler foreach loop bellow.

//foreach ($buses as &$value){   //this uses a reference to the elements of the array. the digit indexed elements of the array get replace with an array containing the corresponding bus info
//$bus= new busline($value);    //this could then be passed, again by reference, to a new array indexed by bus name.
//$value=$bus->get_busInfo();}

$this->master=array();
foreach ($this->buses as $value){
$this->bus= new busline($value);
$this->master[$value]=$this->bus->get_busInfo();
$this->buses= json_encode($this->master);

unset($this->bus);

}}



public function getBuses(){
$this->run();
return $this->buses;}

?>

}
