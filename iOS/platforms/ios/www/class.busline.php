<?
//class.busline.php
class busline
{
private $master;
private $vehicle;
private $context;
private $xml;
private $url;
private $path;


function __construct($bus, $dir)
{
//$this->bus = $bus;
$this->bus=strtoupper($bus);
$this->bus=str_replace("+", "%2b", $this->bus);
$this->dir=$dir;




//$this->xml = simplexml_load_file("http://bustime.mta.info/api/siri/vehicle-monitoring.xml?key=ef4e5e00-06e5-4bea-b58b-b42a41fa3e0b&OperatorRef=MTA%20NYCT&LineRef=MTA%20NYCT_".$this->bus);
}


private function download_page2($path){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$path);
	curl_setopt($ch, CURLOPT_FAILONERROR,1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 15);
	$retValue = curl_exec($ch);			 
	curl_close($ch);
	return $retValue;
}





private function process()    //create an associative array with each property for each bus
{
    
$this->path="http://bustime.mta.info/api/siri/vehicle-monitoring.xml?key=ef4e5e00-06e5-4bea-b58b-b42a41fa3e0b&OperatorRef=MTA%20NYCT&LineRef=MTA%20NYCT_".$this->bus."&DirectionRef=".$this->dir;
//echo $this->path;

$sXML = $this->download_page2($this->path);
$this->xml=new SimpleXMLElement($sXML);
//$this->xml = simplexml_load_file("http://bustime.mta.info/api/siri/vehicle-monitoring.xml?key=ef4e5e00-06e5-4bea-b58b-b42a41fa3e0b&OperatorRef=MTA%20NYCT");


//foreach ($this->xml->{'ServiceDelivery'}->{'VehicleMonitoringDelivery'}->{'VehicleActivity'} as $this->vehicle){
  //  array_push($this->master, (string)$this->vehicle->{'MonitoredVehicleJourney'}->{'PublishedLineName'});  
    
    
    
   ///// 
$this->master=array();
foreach ($this->xml->{'ServiceDelivery'}->{'VehicleMonitoringDelivery'}->{'VehicleActivity'} as $this->vehicle){
$this->ID=trim($this->vehicle->{'MonitoredVehicleJourney'}->{'VehicleRef'});

$this->master['bus'][$this->ID]['PublishedLineName']=(string)$this->vehicle->{'MonitoredVehicleJourney'}->PublishedLineName;
$this->master['bus'][$this->ID]['VehicleRef']=(string)$this->vehicle->{'MonitoredVehicleJourney'}->VehicleRef;
$this->master['bus'][$this->ID]['Direction']=(string)$this->vehicle->{'MonitoredVehicleJourney'}->DirectionRef;
$this->master['bus'][$this->ID]['DestinationName']=(string)$this->vehicle->{'MonitoredVehicleJourney'}->DestinationName;
$this->master['bus'][$this->ID]['Longitude']=(string)$this->vehicle->{'MonitoredVehicleJourney'}->VehicleLocation->Longitude;
$this->master['bus'][$this->ID]['Latitude']=(string)$this->vehicle->{'MonitoredVehicleJourney'}->VehicleLocation->Latitude;
$this->master['bus'][$this->ID]['Bearing']=(string)$this->vehicle->{'MonitoredVehicleJourney'}->Bearing;
$this->master['bus'][$this->ID]['ProgressRate']=(string)$this->vehicle->{'MonitoredVehicleJourney'}->ProgressRate;
$this->master['bus'][$this->ID]['JourneyPatternRef']=(string)$this->vehicle->{'MonitoredVehicleJourney'}->JourneyPatternRef;
}
}

public function get_busInfo(){
$this->process();
return $this->master;}

//function __destruct()
//{
//echo " Object Destroyed."; 
//}


}
?>