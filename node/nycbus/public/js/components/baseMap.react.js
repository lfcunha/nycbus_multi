/**
 * Created by luiscunha on 12/20/14.
 */

//var cl=require('../actions/stops');

function mapFactory(component, createdCallback, createMap){
    if (!component.state.initialized) {
        createMap(component)
    }
    else {
        createdCallback(component.state.map)
    }

}

function create_map(component){
    map=new google.maps.Map(component.refs.mapCanvas.getDOMNode(),component.state.n);
    component.setState({ map: map, initialized:true });

    $(window).resize(function(){
        $("#map-canvas").css("height", $(window).height());
        $("#map-canvas").css("width", $(window).width())
        google.maps.event.trigger(map, 'resize');
        map.setZoom( map.getZoom() );
    });
}

function createdmap(){};





var MapCanvas = React.createClass({

    centerAtLocation: function (pos) {
        mapFactory(this, function(map){map.setCenter(pos); map.setZoom(16); $("#progress").empty() //stop the progress spin wheel
         })
    },

    centerAtMyLocation: function () {
        var self=this;
        var myLocationArray= new Array();
        var reddot = "nycbus/images/reddot10.png";
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (e) {
                self.state.centroid = e.coords.latitude +","+e.coords.longitude;
                pos = new google.maps.LatLng(e.coords.latitude, e.coords.longitude);
                if (!(pos.lat() > 44.001 || pos.lat() < 38.0177 || pos.lng() < -80.00455 || pos.lng() > -70.045)) {
                    self.centerAtLocation(pos);
                    var reddot = "nycbus/images/reddot10.png";
                    var currCenter=map.getCenter();
                    var centerMarker=new google.maps.Marker({position:currCenter,icon:reddot,zIndex:100});
                    if (self.state.centerMarker!=null){
                        self.state.centerMarker.setVisible(false);
                        self.state.centerMarker.setMap(null);
                    }
                    self.setState({centerMarker:centerMarker});
                    self.state.centerMarker.setMap(map);
                    self.showStops();
                }
            })
        }
    },

    showStops: function (){
        var self = this;
        //http://lfcunha.nyc/nycbusapi/stops/40.67616228518975,-73.98325460000001/0
        var url = "http://lfcunha.nyc/nycbusapi/stops/"+self.state.centroid+"/"+self.state.direction;
        console.log(url);
        $.getJSON(url,function(e){
            console.log(e);
        })
    },


    getInitialState: function(){
        var direction=0;
        var centroid = null;
        var center= null;
        var centerMarker = null;
        var initialized = false;
        var mapProperties = {
            disableDoubleClickZoom:true,
            zoom:11,
            disableDefaultUI:true,
            mapTypeControl:false,
            zoomControl:false,
            styles:[{featureType:"poi", stylers:[{visibility:"off"}]}],
            suppressInfoWindows:true,
            overviewMapControl:true,
            overviewMapControlOptions:{opened:false},
            streetViewControl:false,
            center: { lat: 40.853112, lng: -73.905523},
            mapTypeId:google.maps.MapTypeId.ROADMAP
        }

        return {
            n:mapProperties, initialized:initialized, centerMarker:centerMarker,center:center, centroid:centroid, direction:direction
        }
    },

    componentDidMount: function (){
        mapFactory(this, createdmap, create_map)
        var map = this.state.map;
        this.centerAtMyLocation();
        this.showStops();
    },

    render: function () {
        var mapStyle = {
            margin: '0 0 0 0',
            overflow: 'auto',
            position: 'absolute',
            height: $(document).height(),
            width: '100%'
        }

        return (
            <div ref="mapCanvas" id="map-canvas" style={mapStyle} ></div>
        );
    }
});



var map_=React.render(
    <MapCanvas />,
    document.getElementById('wrapper2')
);


