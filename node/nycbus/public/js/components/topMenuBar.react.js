

var UpperLeft = React.createClass({
    handleClick: function() {
        map_.centerAtMyLocation();
    },

    render: function() {
        var leftMenuStyle =  {
            marginLeft:'10px',
            top: "5px",
            overflow: 'auto',
            position:'absolute',
            height:"50px",
            width:"120px",
            zIndex:"9999"
        }
        var buttonGroup = {
            backgroundColor: "rgba(255, 255, 255, 0.0)",
            position:"absolute",
            width:"100px"
        }

        return (
            <div className="btn-toolbar" id="leftMenu" style={leftMenuStyle} >
                <div className="btn-group" style={buttonGroup} >
                    <button type="button" className="btn" id="slide" ><i className="icon-reorder" ></i></button>
                    <button type="button" className="btn" id="myLocation" onClick={this.handleClick} ><i className="icon-location-arrow" ></i></button>
                </div>
            </div>
        );
    }
});



var UpperRight = React.createClass({

    handleClick: function(event) {
       // this.setState({liked: !this.state.liked});
        alert("a");
    },

    render: function () {
        var rightMenuStyle =  {
            marginTop:'0px',
            top: "5px",
            position:'absolute',
            float: "right",
            height:"50px",
            marginLeft:"10px",
            width:"100%"
        }
        var buttonGroup = {
            backgroundColor: "rgba(255, 255, 255, 0.0)",
            position:"relative",
            width:"120px",
            marginRight: "10px",
            float: "right"
        }
        return (
            <div className="btn-toolbar" id="rightMenu"  style={rightMenuStyle} >
                <div className="btn-group" style={buttonGroup}>
                    <button type="button" id="reverseDirection" className="btn" onClick={this.handleClick}><i className="icon-exchange"></i></button>
                    <button type="button" id="myeye" className="btn"><i className="icon-pushpin"></i></button>
                    <button type="button" id="showFav" className="btn"><i className="icon-star-empty"></i></button>
                </div>
            </div>
        );
    }
});

var Progress = React.createClass({

    getInitialState : function(){
            var opts = {
                lines: 15, // The number of lines to draw
                length: 20, // The length of each line
                width: 10, // The line thickness
                radius: 28, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#000', // #rgb or #rrggbb or array of colors
                speed: 0.8, // Rounds per second
                trail: 63, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: '50%', // Top position relative to parent
                left: '50%' // Left position relative to parent
            };

            return {options:opts}

    },

    componentDidMount: function (){
        $("#progress").css("margin-top", $(window).height()/2);
        $("#progress").css("margin-left", $(window).width()/2);
        var spinner = new Spinner(this.state.options).spin(document.getElementById('progress'));

    },

    render: function () {
        var styleProgress = {
            marginTop: "0px",
            marginLeft: "0px",
            position: "absolute",
            zIndex: "999999"
        }
        var styleParagraph = {
            marginTop:"200px",
            color:"#ff0000"

        }

        return (
            <div>
                <div id="progress" style={styleProgress} >
                    <p style={styleParagraph}>Wait while we aquire your position </p>
                    <p></p>
                </div>

            </div>
            );
    }

});

var TopMenuBar = React.createClass({
    render: function() {
        return (
            <div>
                <UpperLeft />
                <UpperRight />
                <Progress />
            </div>
        );
    }
});

React.render(
    <TopMenuBar />,
    document.getElementById('topMenuBar')
);




