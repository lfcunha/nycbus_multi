/**
 * Created by luiscunha on 1/2/15.
 */



function centerAtMyLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (e) {
            pos = new google.maps.LatLng(e.coords.latitude, e.coords.longitude);

            if (!(pos.lat() > 44.001 || pos.lat() < 38.0177 || pos.lng() < -80.00455 || pos.lng() > -70.045)) {
                map.setCenter(pos);
                map.setZoom(16);

                //currCenter = map.getCenter();
                var r = new google.maps.Marker({position: pos, icon: reddot, zIndex: 100});
                if (myLocationArray.length > 0) {
                    myLocationArray[0].setVisible(false);
                    myLocationArray[0].setMap(null);
                    myLocationArray.length = 0
                }
                myLocationArray.push(r);
                myLocationArray[0].setMap(map);
                $("#progress").empty(); //stop the progress spin wheel
                setStop(pos);
            }
        })
    }
}

var reddot="nycbus/images/reddot10.png";

var setCenter = function(){
    currCenter=map.getCenter();
    var r=new google.maps.Marker({position:currCenter,icon:reddot,zIndex:100});
    if(myLocationArray.length>0){
        myLocationArray[0].setVisible(false);
        myLocationArray[0].setMap(null);
        myLocationArray.length=0
    }
    myLocationArray.push(r);
    myLocationArray[0].setMap(map);

}


module.exports.centerAtMyLocation=centerAtMyLocation();
