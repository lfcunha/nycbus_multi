/**
 * Created by luiscunha on 2/24/15.
 */


var stops = function (){
    console.log("cen: " + cen);
    function e(){
        $.getJSON("http://nycbus.us/getstops3.php?center="+cen+"&dir="+direction,function(e){
            items.length=0;
            console.log("items3: "+items);
            $.each(e.buslist,function(e,t){
                items.push(t);
            });
            console.log("items4: "+items);
            $.each(e,function(t){
                var n="labels";
                if(e[t].buses)var r=e[t].buses.split(",").length;
                n+=r;
                var i=22*r;
                var s=new MarkerWithLabel({
                    position: new google.maps.LatLng(e[t].lat,e[t].lon),
                    icon: stopicon,
                    htmlStop: e[t].buses,
                    stopNumber:e[t].stop,
                    labelContent:e[t].buses,
                    labelClass:n,
                    labelAnchor:new google.maps.Point(i,0),
                    labelStyle:{opacity:.75},
                    size:new google.maps.Size(12,12),zIndex:100});
                var o=[];
                o[0]=String(s.htmlStop);
                var u=o;
                stopsArray.push(s);
                google.maps.event.addListener(s,"click",function(){
                    revertSearchB();
                    hideAddressBar();
                    document.getElementById("appendedInputButton-03").style.width="98%";
                    document.getElementById("searchB").style.display="none";
                    var n=e[t].buses.split()[0];
                    var r=n.split(",");
                    routesMenu(r);
                    //legacy code from when not all buses were being tracked by GPS
                    //needed to filter buses at stop by the bus list of buslines with GPS
                    //var i=_.intersection(r,active);
                    //var s=_.difference(r,_.intersection(r,active));
                    var s=r;
                    for(var o=0;o<s.length;o++){
                        s[o]=s[o].replace(/\+/g,"%2b")}
                    var u=stopTimes(e[t].stop,s);
                    console.log("stop: "+e[t].stop);
                    toggleRoutesOn();
                    document.getElementById("appendedInputButton-03").style.color="#555555";
                    document.getElementById("appendedInputButton-03").style.color="#C0C0C0";
                    document.getElementById("appendedInputButton-03").value="Loading arrival times...";
                    $(".open").pageslide();
                });
                s.setMap(map);
                console.log(s);
            });
            addRoutes(items);
            busMarkers.clearOverlays();
            busMarkers.markersArray.length=0;
            init.init(items);
            console.log("12: "+items);
            setTimeout(function(){
                busMarkers.showOverlays()},1e3)
        })
    } //e()
    (function(){checkMemory()})();
    if(map)var t=map.getZoom();
    if(t>15){
        if(!bounds)getBounds();
        clearStops();
        e();
    }
} //stops

module.exports = stops;