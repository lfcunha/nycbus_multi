/**
 * Created by luiscunha on 1/2/15.
 */
"use strict";
var Backbone = require('backbone');
var $ = require('jquery-browserify');
Backbone.$ = $;

module.exports = Backbone.Model.extend({
    initialize: function(){
        //alert("start");
    },
    // Set default values.
    defaults: {
        stop: '',
        buses: '',
        lat:'',
        lon:'',
        dir:''},
    url: function(id){
        return 'http://lfcunha.nyc/nycbusapi/stops/40.676181899999996,-73.98319850000001/0'
    }


});