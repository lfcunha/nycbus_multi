var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'NYCBUS' });
  //res.send('hello world');
});

/*router.get('/users', function(req, res) {
  res.render('test', { title: 'Express!' });
});
*/


module.exports = router;
