var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res) {
  res.send('respond with a resource');
});
router.get('/a/:name/:id', function(req, res) {
  res.send(req.param('id'));
});

module.exports = router;
