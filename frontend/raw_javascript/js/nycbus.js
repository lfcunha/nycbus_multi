/**
 * Created by luiscunha on 8/31/15.
 */
var app = window.app || {};

(function ($) {
    app.test = function(){console.log("testing app")}
    ua = navigator.userAgent;
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-42381246-1', 'nycbus.us');
    ga('send', 'pageview');

    <!-- end Google Analytics -->

    //Get Ajax requests with promises
    function get(url) {
        // Return a new promise.
        return new Promise(function (resolve, reject) {
            // Do the usual XHR stuff
            var req = new XMLHttpRequest();
            req.open('GET', url);
            req.onload = function () {
                // This is called even on 404 etc
                // so check the status
                if (req.status == 200) {
                    // Resolve the promise with the response text
                    resolve(req.response);
                }
                else {
                    // Otherwise reject with the status text
                    // which will hopefully be a meaningful error
                    reject(Error(req.statusText));
                }
            };

            // Handle network errors
            req.onerror = function () {
                reject(Error("Network Error"));
            };

            // Make the request
            req.send();
        });
    }

    //Get JSON from Ajax requests through promises
    function getJSON(url) {
        return get(url).then(JSON.parse);
    }


    /*
     getJSON("http://nycbus.us/getStopTimes.php?stop=402677").then(function(response) {
     console.log("Yey JSON!", response);
     });
     */

    (function updateLocalStorage() {
        if (localStorage["active"]) {
            if (!localStorage["version"] || localStorage["version"] != "v1") {
                console.log("needs update");
                delete window.localStorage["active"];
                localStorage["version"] = "v1";
                alert("Iphone User: Re-add this app to Home Screen for UPDATE!");
            }
            console.log("up to date");
        }
    })();

    function hideAddressBar() {
        if (!window.location.hash) {
            if (document.height < window.outerHeight) {
                document.body.style.height = window.outerHeight + 50 + "px"
            }
            setTimeout(function () {
                window.scrollTo(0, 1)
            }, 50)
        }
    }

    function onBodyLoad() {
        hideAddressBar();
        favOn();
        $("#appendedInputButton-03").click(function () {
            console.log("search clicked");
            toggleSearch();
            toggleRoutesOff()
        });
        if (window.DeviceOrientationEvent) {
            window.addEventListener("orientationchange", function () {
                hideAddressBar();
                var e = window.matchMedia("(orientation: portrait)");
                if (e.matches) { //portrait
                    if (screen.width < 321) {
                        $("#appendedInputButton-03").css("width", "98%")
                    }
                    else if (screen.width > 320) {

                        $("#appendedInputButton-03").css("width", "98%")
                    }
                    else if (screen.width > 599) {
                        $("#appendedInputButton-03").css("width", "98%")
                    }
                    $("#map-canvas").css("width = 100%")
                }
                else { //landscape
                    if (screen.width < 321) {
                        $("#appendedInputButton-03").css("width", "99%");
                    }
                    else if (screen.width > 320) {
                        $("#appendedInputButton-03").css("width", "99%");
                    }
                    else if (screen.width > 900) {
                        $("#appendedInputButton-03").css("width", "99%");
                    }
                    $("#map-canvas").css("width = 100%");
                    $("#html").css("width = 100%");
                    $("#body").css("width = 100%")
                }
            })
        }
    }

    function timer() {
        alert("Monitoring:B63 \nStop: Union st/5th ave\nTime:10:00pm\nArrival Alert:10min prior")
    }

    function favRemove(e) {
        if (confirm("delete " + e + "?")) {
            var t = JSON.parse(localStorage["favs"]);
            for (var n = 0; n < t.length; n++) {
                if (t[n] == e)t.splice(n, 1)
            }
            localStorage["favs"] = JSON.stringify(t)
        }
        favOn();
        alert("Removed " + e + ". \n Close side menu.")
    }

    function showFavs() {
        var e = map.getZoom();
        if (e > 15) {
            if (document.getElementById("showFavs").innerHTML == '<i class="fa fa-star-o"></i>') {
                document.getElementById("showFavs").innerHTML = '<i class="fa fa-star"></i>';
                var t = JSON.parse(localStorage["favs"]);
                console.log(t)
                activateFavs(t)
            } else {
                document.getElementById("showFavs").innerHTML = '<i class="fa fa-star-o"></i>';
                lookHere()
            }
        }
    }

    app.myprompt = function() {
        console.log("prompt called", " ");
        var e = prompt("Enter route name:");
        console.log(e);
        if (e)setFavs(e)
    }

    function setFavs(e) {
        if (Modernizr.localstorage) {
            var n = e;
            var r = "no";
            var i = "no";
            if (n != "") {
                var i = "yes"
                if (i == "yes") {
                    var o = [];
                    if (!localStorage["favs"]) {
                        o.push(n);
                        localStorage["favs"] = JSON.stringify(o)
                    } else {
                        var u = JSON.parse(localStorage["favs"]);
                        for (var s = 0; s < u.length; s++) {
                            if (n == u[s]) {
                                r = "yes";
                                break
                            }
                        }
                        if (r != "yes") {
                            Promise.resolve(function(){
                                u.push(n);
                                localStorage["favs"] = JSON.stringify(u);
                            }).then(showFavs());
                            alert("added " + n + " to favorites.");
                            favOn()
                        }
                    }
                }
            }
            else {
                return false
            }
        }
    }

    function fixInfoWindow() {
        var e = google.maps.InfoWindow.prototype, t = e.open;
        e.open = function (e, n, r) {
            if (r) {
                return t.apply(this, arguments)
            }
        }

    }

    function lookHere() {
        items.length = 0;
        busMarkers.clearOverlays();
        busMarkers.markersArray.splice(0, 1);
        bounds = null;
        console.log("b: " + bounds);
        document.getElementById("appendedInputButton-03").value = "Touch stop for schedule ";
        document.getElementById("appendedInputButton-03").style.color = "#C0C0C0";
        emptyRoutesTab(1, function () {
            stops();
            update_bus_position.init(items)
        });
        document.getElementById("showFavs").innerHTML = '<i class="fa fa-star-o"></i>';
    }

    function reverseDirection() {
        var posstring = "reversed direction";
        $.getJSON("http://nycbus.us/setLocation.php?q=" + posstring, function (e) {
        });
        console.log("reversed");
        var e = map.getZoom();
        if (e > 15) {
            toggleRoutesOff();
            document.getElementById("appendedInputButton-03").value = "Search / Info";
            document.getElementById("appendedInputButton-03").style.color = "#C0C0C0";
            if (direction == 0) {
                direction = 1
            }
            else {
                direction = 0
            }
            stops();
            addRoutes(items);
            busMarkers.clearOverlays();
            busMarkers.markersArray.splice(0, 1);
            update_bus_position.init(items);
            setTimeout(function () {
                busMarkers.showOverlays()
            }, 1e3);
        }
    }

    function toggleRoutes() {
        var e = document.getElementById("routes");
        if (e.style.display == "block") {
            e.style.display = "none"
        }
        else {
            e.style.display = "block"
        }
    }

    function toggleRoutesOn() {
        var e = document.getElementById("routes");
        if (e.style.display == "none") {
            e.style.display = "block"
        }
    }

    function toggleRoutesOff() {
        var e = document.getElementById("routes");
        if (e.style.display == "block") {
            e.style.display = "none"
        }
    }

    function setLocation(id, callback){
        document.getElementById("showFavs").className = "btn";
        if (my_position != null)map.setCenter(my_position);
        map.setZoom(16);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (e) {
                function center_and_look(center) {
                    map.setCenter(center);
                    callback() //lookhere
                }

                accuracy = e.coords.accuracy;
                my_position = new google.maps.LatLng(e.coords.latitude, e.coords.longitude);
                var posstring = "setLocation: " + e.coords.latitude + "," + e.coords.longitude;
                $.getJSON("http://nycbus.us/setLocation.php?q=" + posstring, function (e) {
                }); //logging

                if (my_position.lat() > 44.001 || my_position.lat() < 38.0177 || my_position.lng() < -80.00455 || my_position.lng() > -70.045) {
                    my_position.lat() = 40.853112;
                    my_position.lng() = -73.905523
                }
                center_and_look(my_position);
                currCenter = map.getCenter();
                var r = new google.maps.Marker({position: my_position, icon: reddot, zIndex: 100});
                if (myLocationArray.length > 0) {
                    myLocationArray[0].setVisible(false);
                    myLocationArray[0].setMap(null);
                    myLocationArray.length = 0
                }
                myLocationArray.push(r);
                myLocationArray[0].setMap(map);
                map.setZoom(16)
                posstring = my_position.lat() + "," + my_position.lng();
                $.getJSON("http://nycbus.us/setLocation.php?q=" + posstring + "&&ua=" + ua + "&&sz=" + screen.width, function (e) {
                });
            }, function () {
                handleNoGeolocation(true)
            })

        }
        else {
            handleNoGeolocation(false)

        }
        //return true;
    }

    function handleNoGeolocation(e) {
        if (e) {
            var t = "Error: The Geolocation service failed."
        }
        else {
            var t = "Error: Your browser doesn't support geolocation."
        }
        var n = {map: map, position: new google.maps.LatLng(40.853112, -73.905523), content: t};
        var r = new google.maps.InfoWindow(n);
        map.setCenter(n.position);
        currCenter = map.getCenter()
    }

    app.setTransit = function() {
        if (set2 == null) {
            transitLayer = new google.maps.TransitLayer;
            transitLayer.setMap(map);
            set2 = "yes"
        } else {
            set2 = null;
            transitLayer.setMap(null)
        }
    }

    app.setTraffic = function() {
        console.log("traffic");
        if (set == null) {
            trafficLayer = new google.maps.TrafficLayer;
            trafficLayer.setMap(map);
            set = "yes"
        } else {
            set = null;
            trafficLayer.setMap(null)
        }
    }

    function codeAddress() {
        var e = document.getElementById("address").value;
        geocoder.geocode({address: e}, function (e, t) {
            if (t == google.maps.GeocoderStatus.OK) {
                map.setCenter(e[0].geometry.location);
                var n = new google.maps.Marker({map: map, position: e[0].geometry.location})
            } else {
                alert("Geocode was not successful for the following reason: " + t)
            }
        })
    }

    function requestBus() {
        document.getElementById("showFavs").innerHTML = '<i class="fa fa-star-o"></i>';
        emptyRoutesTab();
        items.length = 0;
        var e = $("input#appendedInputButton-03").val();
        if (e != "") {
            clearStops();
            items.push(e);
            addRoutes(items, "yes");
            update_bus_position.init(items);
            map.setZoom(13);
            if (stopsArray) {
                for (i in stopsArray) {
                    stopsArray[i].setVisible(false)
                }
            }
            clearStops();
        }
    }

    app.activateBus = function() {
        var e = $("#activateBus").attr("name")
        console.log("activated " + e)
        var posstring = "activateBus: " + e;
        $.getJSON("http://nycbus.us/setLocation.php?q=" + posstring, function (e) {
        });
        hideAddressBar();
        document.getElementById("appendedInputButton-03").style.color = "#555555";
        var t = map.getZoom();
        if (t > 15) {
            if (stopsArray) {
                for (i in stopsArray) {
                    if (String(stopsArray[i].htmlStop).indexOf(e) === -1)stopsArray[i].setVisible(false);
                    else stopsArray[i].setVisible(true)
                }
            }
        }
        busMarkers.clearOverlays();
        busMarkers.markersArray.length = 0;
        itemsSingle.length = 0;
        var n = e;
        if (n != "M15+") {
            var r = n.replace(/\+/g, "-SBS")
        }
        else r = n;
        console.log("Activate ee: " + r);
        itemsSingle.push(e);
        update_bus_position.init(itemsSingle);
        addRoutes(itemsSingle);
        if (times3 != undefined) {
            if (times3[r] != undefined) {
                if (times3[r].length == 0)document.getElementById("appendedInputButton-03").value = "No Buses on Route.";
                else if (times3[r][0].length > 2) {
                    document.getElementById("appendedInputButton-03").value = "Schedule (hr:min): " + _.uniq(times3[r])
                }
                else {
                    document.getElementById("appendedInputButton-03").value = r + " in (min): " + _.uniq(times3[r])
                }
            }
            else {
                document.getElementById("appendedInputButton-03").value = "No Buses on Route"
            }
        }
        else {
            document.getElementById("appendedInputButton-03").value = "Select Bus Stop"
        }
    }//activateBus

    function showAll() {
        if (stopsArray) {
            for (i in stopsArray) {
                stopsArray[i].setVisible(true)
            }
        }
        busMarkers.clearOverlays();
        update_bus_position.init(items);
        addRoutes(items);
        document.getElementById("appendedInputButton-03").style.color = "#C0C0C0";
        document.getElementById("appendedInputButton-03").value = "Select route.";
        console.log(items)
    }

    function activateFavs(e) {
        busMarkers.clearOverlays();
        busMarkers.markersArray.length = 0;
        items = [];
        for (var t = 0; t < e.length; t++) {
            items.push(e[t])
        }
        clearStops();
        update_bus_position.init(items);
        addRoutes(items)
    }

    function getBounds() {
        bounds = map.getBounds();
        if (bounds) {
            ne = bounds.getNorthEast();
            sw = bounds.getSouthWest();
            nw = new google.maps.LatLng(ne.lat(), sw.lng());
            se = new google.maps.LatLng(sw.lat(), ne.lng());
            cenlat = (ne.lat() + se.lat()) / 2;
            cenlon = (ne.lng() + nw.lng()) / 2;
            cen = cenlat + "," + cenlon
        }
    }

    function array_unique(e) {
        var t = [];
        for (var n = 0; n < e.length; n++) {
            if (t.indexOf(e[n]) == -1) {
                t.push(e[n])
            }
        }
        return t
    }

    function emptyRoutesTab(id, callback) {
        node = document.getElementById("routemenu");
        while (node.hasChildNodes()) {
            node.removeChild(node.lastChild);
            routeMenu.length = 0
        }
        if (callback != undefined)callback();
    }

    function routesMenu(e) {
        emptyRoutesTab();
        $.each(e, function (e, n) {
            t = JSON.parse(localStorage[n]);
            console.log("t: " + t.color);
            if (routeMenu.indexOf(n) < 0) {
                var r = document.getElementById("routemenu");
                var i = document.createElement("Button");
                if (routeMenu.length == 4 || routeMenu.length == 8 || routeMenu.length == 12) {
                    var s = document.createElement("br");
                    r.appendChild(s)
                }
                if (n !== "") {
                    var o = n;
                    if (active.indexOf(n) != -1)o = '<img src="images/gps1.ico">' + n;
                    i.innerHTML = o;
                    i.setAttribute("type", "button");
                    i.setAttribute("class", "btn");
                    var u = "app.activateBus('" + n + "')";
                    var a = "color:" + t.color;
                    i.setAttribute("id", "activateBus");
                    i.setAttribute("name", n);
                    i.setAttribute("onClick", u);
                    i.setAttribute("style", a);
                    r.appendChild(i);
                    routeMenu.push(n);
                }
            }
        })
    }

    function addRoutes(e, t, data) {
        //t = yes or not, for "do center on route, or not"
        function n() {
            if (paths) {
                for (i in paths) {
                    paths[i].setMap(null)
                }
                ;
                paths.length = 0
            }
        }

        function display_routes(result) {
            maxlat = -1000;
            minlat = 1000;
            maxlon = -1000;
            minlon = 1000;
            center = t;
            console.log("center --:", t)
            $.each(result.features, function (line, line_data) {
                //console.log("t1:", line)
                //l2 = line_data;
                //console.log("l2: ", l2)
                var r = [];
                $.each(line_data.coordinates, function (index, coord_pair) {
                    point_ = coord_pair
                    //console.log(point_)
                    var lat = parseFloat(point_[1]);
                    var lon = parseFloat(point_[0]);

                    lat > maxlat ? maxlat = lat : maxlat;
                    lat < minlat ? minlat = lat : minlat;
                    lon > maxlon ? maxlon = lon : maxlon;
                    lon < minlon ? minlon = lon : minlon;

                    point = new google.maps.LatLng(lat, lon);

                    r.push(point)

                })
                if (localStorage[line_data.properties.bus_line]) {
                    console.log("localstorage", JSON.parse(localStorage[line_data.properties.bus_line]).color)
                    var color = JSON.parse(localStorage[line]).color;

                }
                else {
                    var i = direction;
                    var s = {};
                    var o = "dir" + i;
                    console.log(o);
                    s[o] = r;
                    $.getJSON("http://nycbus.us/colors.php?line=" + line_data.properties.bus_line, function (e) {
                        color = e;
                        if (t != "center") {
                            console.log("line:", l2, "color: ", e)
                            //localStorage[l2]=JSON.stringify({color:color,routeGeometry:s}) #add color in start() instead
                        }
                    })
                }

                var n = {path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW};
                line = new google.maps.Polyline({icons: [
                    {icon: n,
                        offset: "5%",
                        repeat: "400px"}
                ],
                    path: r,
                    clickable: false,
                    map: map,
                    strokeColor: color,
                    strokeOpacity: 1,
                    strokeWeight: 2});
                paths.push(line);
                line.setMap(map);
                r = []
            })

            if (center == "yes") {
                console.log("center", minlat, maxlat, minlon, maxlon)
                var n = new google.maps.LatLng(minlat + (maxlat - minlat) / 2, minlon + (maxlon - minlon) / 2);
                map.setCenter(n);
            }
        }

        function r() {
            if (o == "") o = "0";
            console.log("http://nycbus.us/getroutegeometry.php?route=" + u + "&dir=" + o + "&center=" + t);
            var old_url = "http://nycbus.us/getroutegeometry.php?route=" + u + "&dir=" + o + "&center=" + t;
            var new_url = "http://lfcunha.nyc/api/routegeometry/route/" + u + "/direction/" + o + "/center/" + t

            $.getJSON(new_url, function (result) {
                display_routes(result);
            })
        }

        function s() {
            if (paths) {
                for (i in paths) {
                    paths[i].setMap(map);
                }
            }
        }

        var o = direction;
        //if(o==0){o="0"}
        o = o.toString()
        var u = e.toString(); //lines (e.g. B63,B61)
        if (u.indexOf("+") != -1) {
            u = u.replace(/\+/g, "%2b");
        }
        n();
        if (data == undefined) {
            console.log("data1", data)
            r();
        }
        else {
            console.log("data2", data)
            display_routes(data)
        }
        s()
    }

    function clearStops() {
        if (stopsArray) {
            for (i in stopsArray) {
                stopsArray[i].setMap(null);
            }
            stopsArray.length = 0;
        }
    }

    function showStops() {
        if (stopsArray) {
            for (i in stopsArray) {
                stopsArray[i].setMap(map);
            }
        }
    }

    function inArray(e, t) {
        for (var n = e.length; n--;) {
            if (e[n] === t)return true
        }
        return false
    }

    function isEqArrays(e, t) {
        if (e.length !== t.length) {
            return false
        }
        for (var n = e.length; n--;) {
            if (!inArray(t, e[n])) {
                return false
            }
        }
        return true
    }

    function stopTimes(e, t) {
        function n() {
            var posstring = "stop number: " + e;
            $.getJSON("http://lfcunha.nyc/api/stoptimes/stop/" + e, function (e) {
                    console.log(e);
                    if (e.length == 0) document.getElementById("appendedInputButton-03").value = "No Buses on Route.";
                    else {
                        document.getElementById("appendedInputButton-03").style.color = "#555555";
                        var t = "";
                        times3 = e;
                        var n = 0;
                        //if(Object.keys(e)==null) document.getElementById("appendedInputButton-03").value="No Buses Scheduled.";
                        $.each(e, function (e, r) { //e: bus name, r: min to arrival
                                //console.log("ttt: " + r );

                                if (n < 1) {
                                    if (r == "" || r == "undefined") document.getElementById("appendedInputButton-03").value = "No Buses on Route.";
                                    else {
                                        var s = r.join(",");
                                        var o = s.split(",");
                                        if (o[0].length < 3) {
                                            t += e;
                                            t += " in (min)";
                                            t += ": ";
                                            t += _.uniq(r).join(", ");
                                            document.getElementById("appendedInputButton-03").value = t;
                                        }
                                        else { //legacy from when there were lines with no GPS, and so it showed the schedule for those lines (which was a third element in the returned array
                                            t += e;
                                            t += " Schedule (hr:min)";
                                            t += ": ";
                                            t += _.uniq(r).join(", ");
                                            document.getElementById("appendedInputButton-03").value = t;
                                        }
                                        n++;
                                    }
                                }
                                else {
                                    document.getElementById("appendedInputButton-03").value = "Select Route"
                                }
                            }
                        ); //each
                        return e;
                    }
                }
            )//getJSON
        }

        n();
    }

    function revertSearchB() {
        hideAddressBar();
        document.getElementById("appendedInputButton-03").style.width = "98%";
        document.getElementById("searchB").style.display = "none";
        hideAddressBar()
    }

    function checkMemory() {
        var e = 0;
        for (var t in localStorage)e += localStorage[t].length * 2 / 1024 / 1024;
        console.log("mem: " + e);
        if (e > 3) {
            for (var t in localStorage)localStorage.removeItem(t)
        }
    }

    function AutoCenter() {
        e = map.getBounds();
        ne = e.getNorthEast();
        sw = e.getSouthWest();
        var e = new google.maps.LatLngBounds(sw, ne);
        $.each(busMarkers.markersArray, function (t, n) {
            e.extend(n.position)
        });
        map.fitBounds(e)
    }

    function stops() {
        function get_stops_within_radius() {
            if (cen != NaN && cen != undefined) {
                //getJSON("http://nycbus.us/getstops3.php?center="+cen+"&dir="+direction).then(function(e){  //promise
                getJSON("http://lfcunha.nyc/api/start/center/" + cen + "/dir/" + direction).then(function (e) {  //promise
                    //$.getJSON("http://lfcunha.nyc:8889/start/center/"+cen+"/dir/"+direction,function(e){      //callbacks
                    $.each(e.colors, function (e, t) {
                        console.log("colors", e, t)
                        if (!localStorage[e]) localStorage[e] = JSON.stringify({"color": t});
                    });
                    addRoutes("0", "no", e.route_geometry);
                    items.length = 0;
                    console.log("items3: " + items);
                    $.each(e.busline, function (e, t) {
                        items.push(t);
                    });

                    $.each(e.stops, function (t, s) {
                        var n = "labels";
                        if (e.stops[t].buses)var r = e.stops[t].buses.split(",").length;
                        n += r;
                        var i = 22 * r;
                        var s = new MarkerWithLabel({
                            position: new google.maps.LatLng(e.stops[t].lat, e.stops[t].lon),
                            icon: stopicon,
                            htmlStop: e.stops[t].buses,
                            stopNumber: e.stops[t].stop,
                            labelContent: e.stops[t].buses,
                            labelClass: n,
                            labelAnchor: new google.maps.Point(i, 0),
                            labelStyle: {opacity: .75},
                            size: new google.maps.Size(12, 12), zIndex: 100});
                        var o = [];
                        o[0] = String(s.htmlStop);
                        var u = o;
                        stopsArray.push(s);
                        google.maps.event.addListener(s, "click", function () {
                            revertSearchB();
                            hideAddressBar();
                            document.getElementById("appendedInputButton-03").style.width = "98%";
                            document.getElementById("searchB").style.display = "none";
                            var n = e.stops[t].buses.split()[0];
                            var r = n.split(",");
                            routesMenu(r);
                            var s = r;
                            for (var o = 0; o < s.length; o++) {
                                s[o] = s[o].replace(/\+/g, "%2b")
                            }
                            var u = stopTimes(e.stops[t].stop, s);
                            console.log("stop: " + e.stops[t].stop);
                            toggleRoutesOn();
                            document.getElementById("appendedInputButton-03").style.color = "#555555";
                            document.getElementById("appendedInputButton-03").style.color = "#C0C0C0";
                            document.getElementById("appendedInputButton-03").value = "Loading arrival times...";
                            $(".open").pageslide();
                        });
                        s.setMap(map);

                        // display the buses
                        var j = Promise.resolve(
                                $.each(e.buses, function (e, t) {
                                    console.log(e, t);
                                    listOfBuseswithInfoForMarkers.push(t);
                                })).then(function () {
                                busMarkers.clearOverlays();
                                busMarkers.markersArray.splice(0, 1);
                                infowindows.splice(0, 1);
                                busMarkers.markers();
                                busMarkers.showOverlays();
                                showStops();
                            })
                    });


                })
            }
        } //e()
        (function () {
            checkMemory()
        })();
        if (map) var t = map.getZoom();
        if (t > 15) {
            if (!bounds) getBounds();
            clearStops();
            get_stops_within_radius();
        }
    } //stops

    function start() {
        //set the map and zoom listeners
        //set location, request data, and set stops/buses
        //set 30s interval ajax call for bus location update
        var t = [
            {featureType: "poi", stylers: [
                {visibility: "off"}
            ]}
        ];
        google.maps.visualRefresh = true;
        var options = {disableDoubleClickZoom: true, zoom: 16, disableDefaultUI: true, mapTypeControl: false, zoomControl: false, styles: t,
            suppressInfoWindows: true, overviewMapControl: true, overviewMapControlOptions: {opened: false},
            streetViewControl: false, mapTypeId: google.maps.MapTypeId.ROADMAP};
        map = new google.maps.Map(document.getElementById("map-canvas"), options);
        setLocation(1, lookHere);  //set location; lookhere->stops
        setTimeout(function () {
            setInterval(function () {
                update_bus_position.init(items)
            }, 3e4);
        })
        google.maps.event.addListener(map, "zoom_changed", function () {
            zoom = map.getZoom();
            console.log("Zoom: " + zoom);
            if (zoom < 16) {
                document.getElementById("myeye").className = "btn disabled";
                document.getElementById("showFavs").className = "btn disabled";
                document.getElementById("reverseDirection").className = "btn disabled";
                if (stopsArray) {
                    for (i in stopsArray) {
                        stopsArray[i].setVisible(false)
                    }
                }
                if (zoom < 10) {
                    if (paths) {
                        for (i in paths) {
                            paths[i].setVisible(false)
                        }
                    }
                    if (busMarkers.markersArray) {
                        for (i in busMarkers.markersArray) {
                            busMarkers.markersArray[i].setVisible(false)
                        }
                    }
                }
                else {
                    if (paths.length != 0) {
                        for (i in paths) {
                            paths[i].setVisible(true)
                        }
                    }
                    if (busMarkers.markersArray.length != 0) {
                        for (i in busMarkers.markersArray) {
                            if (busMarkers.markersArray[i])busMarkers.markersArray[i].setVisible(true)
                        }
                    }
                }
            }
            else {
                document.getElementById("myeye").className = "btn";
                document.getElementById("showFavs").className = "btn";
                document.getElementById("reverseDirection").className = "btn";
                if (stopsArray) {
                    for (i in stopsArray) {
                        stopsArray[i].setVisible(true);
                    }
                }
            }
        });
    }

    function toggleSearch() {
        console.log("search toggled");
        document.getElementById("appendedInputButton-03").style.width = wid2;
        document.getElementById("searchB").style.display = "block";
        setTimeout(hideAddressBar(), 2e3)
    }

    function favOn() {
        var e = document.getElementById("favorite");
        var t = document.createElement("p");
        $("#favorite").empty();
        var n = document.createElement("br");
        t.innerHTML = 'Favorites: <button type="button" id="showFav2" class=""><i class="fa fa-plus"></i></button>';
        e.appendChild(n);
        e.appendChild(t);
        var r = "border:0";
        var i = "width: 80px";
        var s = document.createElement("table");
        s.setAttribute("style", r);
        e.appendChild(s);
        var o = document.createElement("th");
        if (localStorage["favs"]) {
            var u = JSON.parse(localStorage["favs"]);
            u.forEach(function (e) {
                var t = document.createElement("tr");
                var n = e;
                t.innerHTML = "<td id=" + "'" + n + "'" + " style='width:35px; height:40px' onClick='javascript:favRemove(id)'>" + e + "</td>" + '<td><i class="fa fa-times"></i></td>';
                s.appendChild(t)
            })
        }
    }

    var busMarkers = function () {
        var e = new Array;
        listOfBuseswithInfoForMarkers = [];
        //clear overlays
        var t = function () {
            if (e) {
                console.log("called");
                for (i in e) {
                    e[i].setMap(null);
                }
                e.length = 0;
                infowindows.length = 0;
            }
        };
        // show overlays
        var n = function () {
            var t = null;
            if (e) {
                t = new google.maps.InfoWindow({content: "holding...", maxWidth: 150});
                for (i in e) {
                    var n = e[i];
                    var r = null;
                    google.maps.event.addListener(n, "click", function () {
                        var posstring = "Bus clicked: " + this.html2;
                        $.getJSON("http://nycbus.us/setLocation.php?q=" + posstring, function (e) {
                        });
                        hideAddressBar();
                        revertSearchB();
                        document.getElementById("appendedInputButton-03").style.width = "98%";
                        document.getElementById("appendedInputButton-03").style.color = "#555555";
                        document.getElementById("searchB").style.display = "none";
                        hideAddressBar();
                        document.getElementById("appendedInputButton-03").value = this.html2;
                        $(".open").pageslide()
                    });
                    n.setMap(map);
                }
            }
        };

        var r = function (t, n, r, i, s, o) {
            if (t.lng() < 0) {
                marker = new google.maps.Marker({
                    position: t,
                    icon: busicon,
                    zIndex: 100,
                    map: map,
                    dir: n,
                    name: r,
                    dest: i,
                    progress: s,
                    ID: o,
                    html: "<p style='font-size:10px'><strong>" + r + "</strong> to " + i + "</p>",
                    html2: r + " to " + i.toLowerCase()});
                e.push(marker)
            }
        };
        //markers
        var s = function () {
            for (var e in listOfBuseswithInfoForMarkers) {
                var t = new google.maps.LatLng(listOfBuseswithInfoForMarkers[e].Latitude, listOfBuseswithInfoForMarkers[e].Longitude);
                var n = listOfBuseswithInfoForMarkers[e].Direction;
                var i = listOfBuseswithInfoForMarkers[e].PublishedLineName;
                var s = listOfBuseswithInfoForMarkers[e].DestinationName;
                var o = listOfBuseswithInfoForMarkers[e].ProgressRate;
                var u = listOfBuseswithInfoForMarkers[e].VehicleRef;
                r(t, n, i, s, o, u)
            }
        };
        return{
            markersArray: e,
            clearOverlays: function () {
                t()
            },
            showOverlays: function () {
                n()
            },
            addMarker: function (e, t, n, i, s, o) {
                r(e, t, n, i, s, o)
            },
            markers: function () {
                s()
            }
        }
    }(); //busmarkers

    var update_bus_position = function () {
        function start(items) {
            this.item = items;
            listOfBuseswithInfoForMarkers.length = 0;
            $(".open").pageslide();
            var t = items.toString();
            //change + sign in bus names like M34+ to url encoded %2b
            if (t.indexOf("+") != -1) {
                t = t.replace(/\+/g, "%2b")
            }
            //$.getJSON("http://nycbus.us/getsinglebus3.php?bus2=" + t + "&dir=" + direction, function (e) {
            $.getJSON("http://lfcunha.nyc/api/buses/"+ t +"/dir/" + direction, function(e){
                var j = Promise.resolve(
                        $.each(e.buses, function (e, t) {
                            console.log(e, t);
                            listOfBuseswithInfoForMarkers.push(t);
                        })).then(function () {
                        busMarkers.clearOverlays();
                        busMarkers.markersArray.splice(0, 1);
                        infowindows.splice(0, 1);
                        busMarkers.markers();
                        busMarkers.showOverlays();
                        showStops();
                    })
            })
        }

        return  {init: function (items) {
            start(items)
        }}
    }();


    var b = window.matchMedia("(orientation: portrait)");
    var wid = 59;
    if (b.matches) {
        if (screen.width < 321)var wid = 59;
        if (screen.width > 320) {
            var wid = 60
        }
        if (screen.width > 599)var wid = 72;
        if (screen.width > 900)var wid = 80;
        if (screen.width > 1500)var wid = 92
    }
    else {
        if (screen.width < 321)var wid = 77;
        if (screen.width > 320)var wid = 75;
        if (screen.width > 599)var wid = 80;
        if (screen.width > 900)var wid = 88;
        if (screen.width > 1500)var wid = 93
    }

    var wid2 = "59px";
    if (b.matches) {
        if (screen.width < 321)var wid2 = "85%";
        if (screen.width > 320) {
            var wid2 = "88%"
        }
        if (screen.width > 599)var wid2 = "90%";
        if (screen.width > 1500)var wid2 = "92%";
        if (screen.width > 1900)var wid2 = "94%"
    }
    else {
        if (screen.width < 321)var wid2 = "90%";
        if (screen.width > 320)var wid2 = "92%";
        if (screen.width > 599)var wid2 = "93%";
        if (screen.width > 900)var wid2 = "95%";
        if (screen.width > 1500)var wid2 = "96%";
        if (screen.width > 1900)var wid2 = "98%";
    }
    var id;
    var itemsSingle = [];
    var currCenter;
    var accuracy;
    var items;
    direction = 0;
    var map;
    var busicon = "images/busicon2.png";
    var reddot = "images/reddot10.png";
    var stopicon = new google.maps.MarkerImage("images/stop2.png", null, null, null, new google.maps.Size(16, 16));
    var bounds;
    var ne;
    var sw;
    var nw;
    var se;
    active = [];
    var cenlon;
    var cen;
    var i = 0;
    var paths = [];
    var line = [];
    var color = "";
    var my_position = null;
    myLocationArray = [];
    var set;
    var transitLayer;
    var set2;
    var trafficLayer;
    geocoder = new google.maps.Geocoder;
    routeMenu = [];
    var stopsArray = [];
    items = [];
    var times3;
    count = 0;
    infowindows = Array();
    p = 0;
    var stopTriggeringIfNoItems = 0;


    function start_() {
        fixInfoWindow();
        onBodyLoad();
        start();
    }

    google.maps.event.addDomListener(window, "load", start_);

    $("#appendedInputButton-03").click();
    function searchB_() {
        requestBus();
        toggleRoutesOff();
        revertSearchB();
    }

    function reverseDirection_() {
        reverseDirection();
        revertSearchB();
    }

    function myEye_() {
        lookHere();
        toggleRoutesOff();
        revertSearchB();
    }

    function showFavs_() {
        showFavs();
        toggleRoutesOff();
        revertSearchB()
    }

    function setLocation_() {
        setLocation(1, lookHere);
        toggleRoutesOff();
        revertSearchB();
    }

    function pageSlide_() {
        $.pageslide({ direction: 'left', href: '#nav' })
    }

    //button click listeners
    $("#toggle_routes").on("click", toggleRoutesOff);
    $("#show_all").on("click", showAll);
    $("#searchB").on("click", searchB_);
    $("#reverseDirection").on("click", reverseDirection_);
    $("#myeye").on("click", myEye_);
    $("#showFavs").on("click", showFavs_);
    $("#setLocation").on("click", setLocation_);
    $("#pageSlide").on("click", pageSlide_);
})(jQuery);